<a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a>

# 中文总榜 > 软件类 > C++
<sub>数据更新: 2022-01-14&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;温馨提示：中文项目泛指「文档母语为中文」OR「含有中文翻译」的项目，通常在项目的「readme/wiki/官网」可以找到</sub>

|#|Repository|Description|Stars|Updated|
|:-|:-|:-|:-|:-|
|1|[electron/electron](https://github.com/electron/electron)|:electron: Build cross-platform desktop apps with JavaScript, HTML, and CSS|99709|2022-01-13|
|2|[envoyproxy/envoy](https://github.com/envoyproxy/envoy)|Cloud-native high-performance edge/middle/service proxy|18717|2022-01-13|
|3|[taichi-dev/taichi](https://github.com/taichi-dev/taichi)|Productive & portable high-performance programming in Python.|17811|2022-01-13|
|4|[alibaba/weex](https://github.com/alibaba/weex)|A framework for building Mobile cross-platform UI|17810|2021-12-09|
|5|[swoole/swoole-src](https://github.com/swoole/swoole-src)|🚀 Coroutine-based concurrency library for PHP|17211|2022-01-13|
|6|[ossrs/srs](https://github.com/ossrs/srs)|SRS is a simple, high efficiency and realtime video server, supports RTMP, WebRTC, HLS, HTTP-FLV and SRT.|16973|2022-01-13|
|7|[Tencent/mars](https://github.com/Tencent/mars)|Mars is a cross-platform network component  developed by WeChat.|15950|2022-01-13|
|8|[zhongyang219/TrafficMonitor](https://github.com/zhongyang219/TrafficMonitor)|这是一个用于显示当前网速、CPU及内存利用率的桌面悬浮窗软件，并支持任务栏显示，支持更换皮肤。|15551|2022-01-13|
|9|[Tencent/MMKV](https://github.com/Tencent/MMKV)|An efficient, small mobile key-value storage framework developed by WeChat. Works on Android, iOS, macOS, Windows, and POSIX.|14004|2022-01-06|
|10|[Tencent/ncnn](https://github.com/Tencent/ncnn)|ncnn is a high-performance neural network inference framework optimized for the mobile platform|13568|2022-01-13|
|11|[apache/incubator-brpc](https://github.com/apache/incubator-brpc)|Industrial-grade RPC framework used throughout Baidu, with 1,000,000+ instances and thousands kinds of services. "brpc" means "better RPC".|12796|2022-01-13|
|12|[Tencent/rapidjson](https://github.com/Tencent/rapidjson)|A fast JSON parser/generator for C++ with both SAX/DOM style API|11631|2022-01-08|
|13|[Tencent/matrix](https://github.com/Tencent/matrix)|Matrix is a plugin style, non-invasive APM system developed by WeChat.|9440|2021-12-22|
|14|[TarsCloud/Tars](https://github.com/TarsCloud/Tars)|Tars is a high-performance RPC framework based on name service and Tars protocol, also integrated administration platform, and implemented hosting-service via flexible schedule.|9290|2022-01-13|
|15|[vnotex/vnote](https://github.com/vnotex/vnote)|A pleasant note-taking platform.|8858|2022-01-13|
|16|[sonic-pi-net/sonic-pi](https://github.com/sonic-pi-net/sonic-pi)|Code. Music. Live.|8803|2022-01-06|
|17|[DayBreak-u/chineseocr_lite](https://github.com/DayBreak-u/chineseocr_lite)|超轻量级中文ocr，支持竖排文字识别, 支持ncnn、mnn、tnn推理 ( dbnet(1.8M) + crnn(2.5M) + anglenet(378KB)) 总模型仅4.7M |8430|2022-01-13|
|18|[sass/node-sass](https://github.com/sass/node-sass)|:rainbow: Node.js bindings to libsass|8014|2021-12-29|
|19|[ideawu/ssdb](https://github.com/ideawu/ssdb)|SSDB - A fast NoSQL database, an alternative to Redis|7727|2021-12-22|
|20|[vesoft-inc/nebula](https://github.com/vesoft-inc/nebula)|  A distributed, fast open-source graph database featuring horizontal scalability and high availability|7043|2022-01-13|
|21|[PointCloudLibrary/pcl](https://github.com/PointCloudLibrary/pcl)|Point Cloud Library (PCL)|6976|2022-01-13|
|22|[snowie2000/mactype](https://github.com/snowie2000/mactype)|Better font rendering for Windows.|6948|2021-12-06|
|23|[sogou/workflow](https://github.com/sogou/workflow)|C++ Parallel Computing and Asynchronous Networking Engine|6931|2022-01-13|
|24|[hrydgard/ppsspp](https://github.com/hrydgard/ppsspp)|A PSP emulator for Android, Windows, Mac and Linux, written in C++. Want to contribute? Join us on Discord at https://discord.gg/5NJB6dD or just send pull requests / issues. For discussion use the for ...|6833|2022-01-13|
|25|[Tencent/libco](https://github.com/Tencent/libco)|libco is a coroutine library which is widely used in wechat  back-end service. It has been running on tens of thousands of machines since 2013.|6819|2021-12-03|
|26|[drogonframework/drogon](https://github.com/drogonframework/drogon)|Drogon: A C++14/17/20 based HTTP web application framework running on Linux/macOS/Unix/Windows|6714|2022-01-13|
|27|[visualfc/liteide](https://github.com/visualfc/liteide)|LiteIDE is a simple, open source, cross-platform Go IDE. |6693|2022-01-12|
|28|[alibaba/MNN](https://github.com/alibaba/MNN)|MNN is a blazing fast, lightweight deep learning framework, battle-tested by business-critical use cases in Alibaba|6348|2022-01-13|
|29|[Tencent/Hippy](https://github.com/Tencent/Hippy)|Hippy is designed for Web developer to easily build cross-platform and high-performance awesome apps. 👏|6274|2022-01-13|
|30|[Ewenwan/MVision](https://github.com/Ewenwan/MVision)|机器人视觉 移动机器人 VS-SLAM ORB-SLAM2 深度学习目标检测 yolov3 行为检测 opencv  PCL 机器学习 无人驾驶|6195|2021-07-29|
|31|[BYVoid/OpenCC](https://github.com/BYVoid/OpenCC)|Conversion between Traditional and Simplified Chinese|6034|2021-12-18|
|32|[barry-ran/QtScrcpy](https://github.com/barry-ran/QtScrcpy)|Android real-time display control software|5985|2022-01-10|
|33|[idea4good/GuiLite](https://github.com/idea4good/GuiLite)|✔️The smallest header-only GUI library(4 KLOC) for all platforms|5890|2021-08-01|
|34|[PaddlePaddle/Paddle-Lite](https://github.com/PaddlePaddle/Paddle-Lite)|Multi-platform high performance  deep learning inference engine (『飞桨』多平台高性能深度学习预测引擎）|5852|2022-01-13|
|35|[me115/design_patterns](https://github.com/me115/design_patterns)|图说设计模式|5634|2021-08-10|
|36|[AaronFeng753/Waifu2x-Extension-GUI](https://github.com/AaronFeng753/Waifu2x-Extension-GUI)|Video, Image and GIF upscale/enlarge(Super-Resolution) and Video frame interpolation. Achieved with Waifu2x,  Real-ESRGAN, SRMD, RealSR, Anime4K, RIFE, CAIN, DAIN,  and ACNet.|5630|2022-01-07|
|37|[google/sentencepiece](https://github.com/google/sentencepiece)|Unsupervised text tokenizer for Neural Network-based text generation.|5595|2022-01-12|
|38|[xournalpp/xournalpp](https://github.com/xournalpp/xournalpp)|Xournal++ is a handwriting notetaking software with PDF annotation support. Written in C++ with GTK3, supporting Linux (e.g. Ubuntu, Debian, Arch, SUSE), macOS and Windows 10. Supports pen input from  ...|5485|2022-01-13|
|39|[ZLMediaKit/ZLMediaKit](https://github.com/ZLMediaKit/ZLMediaKit)|WebRTC/RTSP/RTMP/HTTP/HLS/HTTP-FLV/WebSocket-FLV/HTTP-TS/HTTP-fMP4/WebSocket-TS/WebSocket-fMP4/GB28181 server and client framework based on C++11|5475|2022-01-13|
|40|[weolar/miniblink49](https://github.com/weolar/miniblink49)|a lighter, faster browser kernel of blink to integrate HTML UI in your app. 一个小巧、轻量的浏览器内核，用来取代wke和libcef|5465|2021-12-27|
|41|[tindy2013/subconverter](https://github.com/tindy2013/subconverter)|Utility to convert between various subscription format|5097|2022-01-13|
|42|[Alinshans/MyTinySTL](https://github.com/Alinshans/MyTinySTL)|Achieve a tiny STL in C++11|4994|2021-12-29|
|43|[qinguoyi/TinyWebServer](https://github.com/qinguoyi/TinyWebServer)|:fire: Linux下C++轻量级Web服务器|4991|2022-01-07|
|44|[wangyu-/udp2raw](https://github.com/wangyu-/udp2raw)|A Tunnel which Turns UDP Traffic into Encrypted UDP/FakeTCP/ICMP Traffic by using Raw Socket,helps you Bypass UDP FireWalls(or Unstable UDP Environment)|4890|2021-12-09|
|45|[duilib/duilib](https://github.com/duilib/duilib)|-|4645|2022-01-11|
|46|[XiaoMi/mace](https://github.com/XiaoMi/mace)|MACE is a deep learning inference framework optimized for mobile heterogeneous computing platforms.|4551|2022-01-13|
|47|[OpenAtomFoundation/pika](https://github.com/OpenAtomFoundation/pika)|Pika is a nosql compatible with redis, it is developed by Qihoo's DBA and infrastructure team|4485|2022-01-03|
|48|[szad670401/HyperLPR](https://github.com/szad670401/HyperLPR)|基于深度学习高性能中文车牌识别 High Performance Chinese License Plate Recognition Framework.|4429|2022-01-13|
|49|[TonyChen56/WeChatRobot](https://github.com/TonyChen56/WeChatRobot)|PC版微信机器人 微信Api、微信api、微信发卡机器人、微信聊天机器人 python微信api 微信接口 微信数据库解密|4166|2021-10-29|
|50|[MegEngine/MegEngine](https://github.com/MegEngine/MegEngine)|MegEngine 是一个快速、可拓展、易于使用且支持自动求导的深度学习框架|4106|2022-01-13|
|51|[OAID/Tengine](https://github.com/OAID/Tengine)|Tengine is a lite, high performance, modular inference engine for embedded device |4077|2022-01-13|
|52|[oceanbase/oceanbase](https://github.com/oceanbase/oceanbase)|OceanBase is an enterprise distributed relational database with high availability, high performance, horizontal scalability, and compatibility with SQL standards.|4062|2022-01-11|
|53|[yedf2/handy](https://github.com/yedf2/handy)|🔥简洁易用的C++11网络库 / 支持单机千万并发连接 / a simple C++11 network server framework|3851|2021-10-11|
|54|[wangyu-/UDPspeeder](https://github.com/wangyu-/UDPspeeder)|A Tunnel which Improves your Network Quality on a High-latency Lossy Link by using Forward Error Correction, possible for All Traffics(TCP/UDP/ICMP)|3726|2021-12-09|
|55|[wang-xinyu/tensorrtx](https://github.com/wang-xinyu/tensorrtx)|Implementation of popular deep learning networks with TensorRT network definition API|3579|2022-01-10|
|56|[HKUST-Aerial-Robotics/VINS-Mono](https://github.com/HKUST-Aerial-Robotics/VINS-Mono)|A Robust and Versatile Monocular Visual-Inertial State Estimator|3360|2021-12-25|
|57|[Tencent/TNN](https://github.com/Tencent/TNN)|TNN: developed by Tencent Youtu Lab and Guangying Lab, a uniform deep learning inference framework for mobile、desktop and server. TNN is distinguished by several outstanding features, including its cr ...|3306|2022-01-13|
|58|[wang-bin/QtAV](https://github.com/wang-bin/QtAV)|A cross-platform multimedia framework based on Qt and FFmpeg(https://github.com/wang-bin/avbuild). High performance. User & developer friendly. Supports Android, iOS, Windows store and desktops. 基于Qt和 ...|3026|2022-01-13|
|59|[baidu/braft](https://github.com/baidu/braft)|An industrial-grade C++ implementation of RAFT consensus algorithm based on brpc,  widely used inside Baidu to build highly-available distributed systems.|3025|2022-01-04|
|60|[Oneflow-Inc/oneflow](https://github.com/Oneflow-Inc/oneflow)|OneFlow is a performance-centered and open-source deep learning framework.|2950|2022-01-13|
|61|[CodingGay/BlackDex](https://github.com/CodingGay/BlackDex)|BlackDex is an Android unpack(dexdump) tool, it supports Android 5.0~12 and need not rely to any environment. BlackDex can run on any Android mobile phone or emulator, you can unpack APK File in sever ...|2917|2022-01-09|
|62|[acidanthera/AppleALC](https://github.com/acidanthera/AppleALC)|Native macOS HD audio for not officially supported codecs|2900|2022-01-13|
|63|[balloonwj/flamingo](https://github.com/balloonwj/flamingo)|flamingo 一款高性能轻量级开源即时通讯软件|2855|2021-10-20|
|64|[feiyangqingyun/QWidgetDemo](https://github.com/feiyangqingyun/QWidgetDemo)|Qt编写的一些开源的demo，预计会有100多个，一直持续更新完善，代码简洁易懂注释详细，每个都是独立项目，非常适合初学者，代码随意传播使用，拒绝打赏和捐赠，欢迎留言评论！|2848|2022-01-13|
|65|[liuchuo/PAT](https://github.com/liuchuo/PAT)|🍭 浙江大学PAT题解(C/C++/Java/Python) - 努力成为萌萌的程序媛～|2768|2021-10-08|
|66|[alphacep/vosk-api](https://github.com/alphacep/vosk-api)|Offline speech recognition API for Android, iOS, Raspberry Pi and servers with Python, Java, C# and Node|2746|2022-01-12|
|67|[alibaba/euler](https://github.com/alibaba/euler)|A distributed graph deep learning framework.|2710|2021-12-30|
|68|[openthread/openthread](https://github.com/openthread/openthread)|OpenThread released by Google is an open-source implementation of the Thread networking protocol|2668|2022-01-13|
|69|[mindspore-ai/mindspore](https://github.com/mindspore-ai/mindspore)|MindSpore is a new open source deep learning training/inference framework that could be used for mobile, edge and cloud scenarios.|2666|2022-01-08|
|70|[Kitt-AI/snowboy](https://github.com/Kitt-AI/snowboy)|Future versions with model training module will be maintained through a forked version here: https://github.com/seasalt-ai/snowboy|2659|2021-10-13|
|71|[hzeller/rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix)|Controlling up to three chains of 64x64, 32x32, 16x32 or similar RGB LED displays using Raspberry Pi GPIO|2577|2022-01-09|
|72|[kungfu-origin/kungfu](https://github.com/kungfu-origin/kungfu)|Kungfu Trader|2549|2021-11-16|
|73|[WasmEdge/WasmEdge](https://github.com/WasmEdge/WasmEdge)|WasmEdge is a lightweight, high-performance, and extensible WebAssembly runtime for cloud native, edge, and decentralized applications. It powers serverless apps, embedded functions, microservices, sm ...|2524|2022-01-13|
|74|[pbek/QOwnNotes](https://github.com/pbek/QOwnNotes)|QOwnNotes is a plain-text file notepad and todo-list manager with markdown support and Nextcloud / ownCloud integration.|2397|2022-01-13|
|75|[Tencent/Tendis](https://github.com/Tencent/Tendis)|Tendis is a high-performance distributed storage system fully compatible with the Redis protocol. |2313|2021-11-18|
|76|[idealvin/cocoyaxi](https://github.com/idealvin/cocoyaxi)|A C++ library from the Namake planet. (go-style coroutine, flag, logging, unit-test, and more)|2306|2022-01-11|
|77|[KwaiAppTeam/KOOM](https://github.com/KwaiAppTeam/KOOM)|KOOM is an OOM killer on mobile platform by Kwai.|2303|2021-11-16|
|78|[BlackINT3/OpenArk](https://github.com/BlackINT3/OpenArk)|OpenArk is an open source anti-rookit(ARK) tool for Windows. |2284|2021-11-25|
|79|[rizonesoft/Notepad3](https://github.com/rizonesoft/Notepad3)|Notepad like text editor based on the Scintilla source code. Notepad3 based on code from Notepad2 and MiniPath on code from metapath. Download Notepad3:|2279|2022-01-12|
|80|[helio-fm/helio-workstation](https://github.com/helio-fm/helio-workstation)|One music sequencer for all major platforms, desktop and mobile|2276|2022-01-10|
|81|[miguelbalboa/rfid](https://github.com/miguelbalboa/rfid)|Arduino RFID Library for MFRC522|2207|2022-01-07|
|82|[rime/librime](https://github.com/rime/librime)|Rime Input Method Engine, the core library|2155|2022-01-13|
|83|[OAID/TengineKit](https://github.com/OAID/TengineKit)|TengineKit - Free, Fast, Easy, Real-Time Face Detection & Face Landmarks & Face Attributes & Hand Detection & Hand Landmarks & Body Detection & Body Landmarks &  Iris Landmarks & Yolov5 SDK On Mobile.|2129|2021-10-18|
|84|[rime/weasel](https://github.com/rime/weasel)|【小狼毫】Rime for Windows|2127|2021-12-15|
|85|[yanyiwu/cppjieba](https://github.com/yanyiwu/cppjieba)|"结巴"中文分词的C++版本|2090|2022-01-09|
|86|[baidu/openrasp](https://github.com/baidu/openrasp)|🔥Open source RASP solution|2077|2022-01-13|
|87|[emilybache/GildedRose-Refactoring-Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata)|Starting code for the GildedRose Refactoring Kata in many programming languages.|2069|2022-01-04|
|88|[yuanming-hu/taichi_mpm](https://github.com/yuanming-hu/taichi_mpm)|High-performance moving least squares material point method (MLS-MPM) solver. (ACM Transactions on Graphics, SIGGRAPH 2018)|2047|2021-11-30|
|89|[Serial-Studio/Serial-Studio](https://github.com/Serial-Studio/Serial-Studio)|Multi-purpose serial data visualization & processing program|1977|2022-01-11|
|90|[kdrag0n/safetynet-fix](https://github.com/kdrag0n/safetynet-fix)|Universal fix for Google SafetyNet on Android devices with hardware attestation and unlocked bootloaders.|1953|2021-12-24|
|91|[Tencent/Hardcoder](https://github.com/Tencent/Hardcoder)|Hardcoder is a solution which allows Android APP and Android System to communicate with each other directly, solving the problem that Android APP could only use system standard API rather than the har ...|1947|2021-11-11|
|92|[seetafaceengine/SeetaFace2](https://github.com/seetafaceengine/SeetaFace2)|SeetaFace 2: open source, full stack face recognization toolkit.|1894|2021-09-28|
|93|[Simple-XX/SimpleKernel](https://github.com/Simple-XX/SimpleKernel)|Simple kernel for learning operating systems. 用于学习操作系统的简单内核|1889|2021-11-24|
|94|[p-ranav/indicators](https://github.com/p-ranav/indicators)|Activity Indicators for Modern C++|1882|2021-10-28|
|95|[DS-Homebrew/TWiLightMenu](https://github.com/DS-Homebrew/TWiLightMenu)|DSi Menu replacement for DS/DSi/3DS/2DS|1856|2022-01-13|
|96|[OpenIntelWireless/IntelBluetoothFirmware](https://github.com/OpenIntelWireless/IntelBluetoothFirmware)|Intel Bluetooth Drivers for macOS|1849|2022-01-01|
|97|[Tatsu-syo/noMeiryoUI](https://github.com/Tatsu-syo/noMeiryoUI)|No!! MeiryoUI is Windows system font setting tool on Windows 8.1/10/11.|1814|2021-12-19|
|98|[wenet-e2e/wenet](https://github.com/wenet-e2e/wenet)|Production First and Production Ready End-to-End Speech Recognition Toolkit|1724|2022-01-12|
|99|[Aloshi/EmulationStation](https://github.com/Aloshi/EmulationStation)|A flexible emulator front-end supporting keyboardless navigation and custom system themes.|1716|2022-01-10|
|100|[Tencent/plato](https://github.com/Tencent/plato)|腾讯高性能分布式图计算框架Plato|1704|2021-08-14|
|101|[SwiftLaTeX/SwiftLaTeX](https://github.com/SwiftLaTeX/SwiftLaTeX)|SwiftLaTeX, a WYSIWYG Browser-based LaTeX Editor |1671|2021-11-03|
|102|[blinker-iot/blinker-library](https://github.com/blinker-iot/blinker-library)|An IoT Solution,Blinker library for embedded hardware. Works with Arduino, ESP8266, ESP32.|1645|2022-01-06|
|103|[microsoft/BlingFire](https://github.com/microsoft/BlingFire)|A lightning fast Finite State machine and REgular expression manipulation library.|1588|2021-11-17|
|104|[facebookresearch/SparseConvNet](https://github.com/facebookresearch/SparseConvNet)|Submanifold sparse convolutional networks|1584|2022-01-06|
|105|[FISCO-BCOS/FISCO-BCOS](https://github.com/FISCO-BCOS/FISCO-BCOS)|FISCO BCOS是由微众牵头的金链盟主导研发、对外开源、安全可控的企业级金融区块链底层技术平台。 单链配置下，性能TPS可达万级。提供群组架构、并行计算、分布式存储、可插拔的共识机制、隐私保护算法、支持全链路国密算法等诸多特性。 经过多个机构、多个应用，长时间在生产环境中的实践检验，具备金融级的高性能、高可用性及高安全性。FISCO BCOS is a secure and reliable  ...|1563|2022-01-13|
|106|[githubhaohao/NDK_OpenGLES_3_0](https://github.com/githubhaohao/NDK_OpenGLES_3_0)|Android OpenGL ES 3.0 从入门到精通系统性学习教程|1542|2021-11-17|
|107|[AntiMicro/antimicro](https://github.com/AntiMicro/antimicro)|Graphical program used to map keyboard buttons and mouse controls to a gamepad. Useful for playing games with no gamepad support|1515|2021-09-19|
|108|[sylar-yin/sylar](https://github.com/sylar-yin/sylar)|C++高性能分布式服务器框架,webserver,websocket server,自定义tcp_server（包含日志模块，配置模块，线程模块，协程模块，协程调度模块，io协程调度模块，hook模块，socket模块，bytearray序列化，http模块，TcpServer模块，Websocket模块，Https模块等, Smtp邮件模块, MySQL, SQLite3, ORM,Redis, ...|1512|2021-12-10|
|109|[SpriteOvO/Telegram-Anti-Revoke](https://github.com/SpriteOvO/Telegram-Anti-Revoke)|Telegram anti-revoke plugin|1499|2021-12-10|
|110|[zhangyuanwei/node-images](https://github.com/zhangyuanwei/node-images)|Cross-platform image decoder(png/jpeg/gif) and encoder(png/jpeg) for Nodejs|1429|2021-11-01|
|111|[zhongyang219/MusicPlayer2](https://github.com/zhongyang219/MusicPlayer2)|这是一款可以播放常见音频格式的音频播放器。支持歌词显示、歌词卡拉OK样式显示、歌词在线下载、歌词编辑、歌曲标签识别、Win10小娜搜索显示歌词、频谱分析、音效设置、任务栏缩略图按钮、主题颜色等功能。 播放内核为BASS音频库(V2.4)。|1410|2021-12-05|
|112|[acidanthera/VirtualSMC](https://github.com/acidanthera/VirtualSMC)|SMC emulator layer|1380|2021-12-06|
|113|[Fluorohydride/ygopro](https://github.com/Fluorohydride/ygopro)|A script engine for "yu-gi-oh!" and sample gui|1371|2022-01-13|
|114|[Tencent/sluaunreal](https://github.com/Tencent/sluaunreal)|lua dev plugin for unreal engine 4|1369|2021-12-02|
|115|[Xtra-Computing/thundersvm](https://github.com/Xtra-Computing/thundersvm)|ThunderSVM: A Fast SVM Library on GPUs and CPUs|1362|2022-01-07|
|116|[netease-im/NIM_Duilib_Framework](https://github.com/netease-im/NIM_Duilib_Framework)|网易云信Windows应用界面开发框架（基于Duilib）。招人招人，windows/mac/duilib/qt/electron http://mobile.bole.netease.com/bole/boleDetail?id=19904&employeeId=510064bce318835c&key=all&type=2&from=timeline|1291|2022-01-04|
|117|[greg7mdp/parallel-hashmap](https://github.com/greg7mdp/parallel-hashmap)|A family of header-only, very fast and memory-friendly hashmap and btree containers.|1242|2021-12-27|
|118|[romkatv/gitstatus](https://github.com/romkatv/gitstatus)|Git status for Bash and Zsh prompt|1232|2022-01-04|
|119|[TianZerL/Anime4KCPP](https://github.com/TianZerL/Anime4KCPP)|A high performance anime upscaler|1226|2021-12-10|
|120|[qicosmos/cinatra](https://github.com/qicosmos/cinatra)|modern c++(c++17), cross-platform, header-only, easy to use http framework|1226|2022-01-13|
|121|[M2Team/NSudo](https://github.com/M2Team/NSudo)|Series of System Administration Tools|1224|2021-11-18|
|122|[loot/loot](https://github.com/loot/loot)|A load order optimisation tool for the Elder Scrolls (Morrowind and later) and Fallout (3 and later) games.|1174|2022-01-12|
|123|[fasiondog/hikyuu](https://github.com/fasiondog/hikyuu)|Hikyuu Quant Framework 基于C++/Python的开源量化交易研究框架|1145|2022-01-13|
|124|[qdtroy/DuiLib_Ultimate](https://github.com/qdtroy/DuiLib_Ultimate)|duilib 旗舰版-高分屏、多语言、样式表、资源管理器、异形窗口、窗口阴影、简单动画|1126|2022-01-04|
|125|[ysc3839/FontMod](https://github.com/ysc3839/FontMod)|Simple hook tool to change Win32 program font.|1122|2021-08-09|
|126|[tencentyun/TRTCSDK](https://github.com/tencentyun/TRTCSDK)|腾讯云TRTC音视频服务，国内下载镜像：|1121|2022-01-12|
|127|[p-ranav/tabulate](https://github.com/p-ranav/tabulate)|Table Maker for Modern C++|1113|2021-10-07|
|128|[WaykiChain/WaykiChain](https://github.com/WaykiChain/WaykiChain)|Public Blockchain as a Decentralized Finance Infrastructure Service Platform|1112|2021-12-11|
|129|[fanvanzh/3dtiles](https://github.com/fanvanzh/3dtiles)|The fastest tools for 3dtiles convert in the world!|1100|2022-01-04|
|130|[ErosZy/WXInlinePlayer](https://github.com/ErosZy/WXInlinePlayer)|🤟Super fast H.264/H.265 FLV player|1094|2021-09-16|
|131|[ZLMediaKit/ZLToolKit](https://github.com/ZLMediaKit/ZLToolKit)|一个基于C++11的轻量级网络框架，基于线程池技术可以实现大并发网络IO|1083|2022-01-13|
|132|[BlueMatthew/WechatExporter](https://github.com/BlueMatthew/WechatExporter)|Wechat Chat History Exporter 微信聊天记录导出程序|1072|2022-01-06|
|133|[sogou/srpc](https://github.com/sogou/srpc)|RPC based on C++ Workflow. Supports Baidu bRPC, Tencent tRPC, thrift protocols.|1072|2022-01-13|
|134|[joyieldInc/predixy](https://github.com/joyieldInc/predixy)|A high performance and fully featured proxy for redis, support redis sentinel and redis cluster|1064|2021-10-01|
|135|[alibaba/graph-learn](https://github.com/alibaba/graph-learn)|An Industrial Graph Neural Network Framework|1004|2021-12-24|
|136|[amov-lab/Prometheus](https://github.com/amov-lab/Prometheus)|Open source software for autonomous drones.|995|2022-01-05|
|137|[Greedysky/TTKMusicplayer](https://github.com/Greedysky/TTKMusicplayer)|TTKMusicPlayer that imitation Kugou music, the music player uses of qmmp core library based on Qt for windows and linux.(支持网易云音乐、QQ音乐、酷我音乐、酷狗音乐)|984|2022-01-09|
|138|[shangjingbo1226/AutoPhrase](https://github.com/shangjingbo1226/AutoPhrase)|AutoPhrase: Automated Phrase Mining from Massive Text Corpora|970|2021-11-18|
|139|[Chuyu-Team/VC-LTL](https://github.com/Chuyu-Team/VC-LTL)|Shared to msvcrt.dll and optimize the C/C++ application file size.|965|2021-12-08|
|140|[devilsen/CZXing](https://github.com/devilsen/CZXing)|C++ port of ZXing and ZBar for Android.|951|2021-08-29|
|141|[Artikash/Textractor](https://github.com/Artikash/Textractor)|Extracts text from video games and visual novels. Highly extensible.|940|2022-01-02|
|142|[opencurve/curve](https://github.com/opencurve/curve)|Curve is a better-used cloud-native SDS storage system, featured with high performance, easy operation, cloud native. Curve is composed with CurveBS and CurveFS. CurveBS provides block devices based o ...|934|2022-01-13|
|143|[ic005k/QtOpenCoreConfig](https://github.com/ic005k/QtOpenCoreConfig)|OpenCore Auxiliary Tools OpenCore Configurator  OCAT|929|2022-01-12|
|144|[cixingguangming55555/wechat-bot](https://github.com/cixingguangming55555/wechat-bot)|带二次开发接口的PC微信聊天机器人|919|2022-01-11|
|145|[Netis/packet-agent](https://github.com/Netis/packet-agent)|A toolset for network packet capture in Cloud/Kubernetes and Virtualized environment.|891|2021-12-22|
|146|[baidu/BaikalDB](https://github.com/baidu/BaikalDB)|BaikalDB, A Distributed HTAP Database.|889|2022-01-13|
|147|[namazso/OpenHashTab](https://github.com/namazso/OpenHashTab)|📝 File hashing and checking shell extension|878|2021-11-30|
|148|[markparticle/WebServer](https://github.com/markparticle/WebServer)|C++  Linux WebServer服务器|835|2021-10-11|
|149|[Tencent/flare](https://github.com/Tencent/flare)| Flare是广泛投产于腾讯广告后台的现代化C++开发框架，包含了基础库、RPC、各种客户端等。主要特点为易用性强、长尾延迟低。 |831|2021-12-24|
|150|[ylmbtm/GameProject3](https://github.com/ylmbtm/GameProject3)|游戏服务器框架，网络层分别用SocketAPI、Boost Asio、Libuv三种方式实现， 框架内使用共享内存，无锁队列，对象池，内存池来提高服务器性能。还包含一个不断完善的Unity 3D客户端，客户端含大量完整资源，坐骑，宠物，伙伴，装备, 这些均己实现上阵和穿戴, 并可进入副本战斗，多人玩法也己实现, 持续开发中。|830|2021-12-10|
|151|[stan-dev/rstan](https://github.com/stan-dev/rstan)|RStan, the R interface to Stan|828|2022-01-12|
|152|[simplefoc/Arduino-FOC](https://github.com/simplefoc/Arduino-FOC)|Arduino FOC for BLDC and Stepper motors - Arduino Based Field Oriented Control Algorithm Library|820|2022-01-08|
|153|[sanni/cartreader](https://github.com/sanni/cartreader)|A shield for the Arduino Mega that can back up video game cartridges.|814|2022-01-13|
|154|[hgneng/ekho](https://github.com/hgneng/ekho)|Chinese text-to-speech engine|813|2022-01-07|
|155|[didi/AoE](https://github.com/didi/AoE)|AoE (AI on Edge，终端智能，边缘计算) 是一个终端侧AI集成运行时环境 (IRE)，帮助开发者提升效率。|803|2021-09-28|
|156|[xmoeproject/KrkrExtract](https://github.com/xmoeproject/KrkrExtract)|A tool that can extract and pack krkr2 and krkrz's xp3 files|797|2022-01-01|
|157|[manticoresoftware/manticoresearch](https://github.com/manticoresoftware/manticoresearch)|Database for search|790|2022-01-13|
|158|[limingfan2016/game_service_system](https://github.com/limingfan2016/game_service_system)|从0开始开发 基础库（配置文件读写、日志、多线程、多进程、锁、对象引用计数、内存池、免锁消息队列、免锁数据缓冲区、进程信号、共享内存、定时器等等基础功能组件），网络库（socket、TCP、UDP、epoll机制、连接自动收发消息等等），数据库操作库（mysql，redis、memcache API 封装可直接调用），开发框架库（消息调度处理、自动连接管理、服务开发、游戏框架、服务间消息收发、消息 ...|776|2021-11-14|
|159|[vesoft-inc/nebula-graph](https://github.com/vesoft-inc/nebula-graph)|A distributed, fast open-source graph database featuring horizontal scalability and high availability|775|2021-09-27|
|160|[WenmuZhou/PytorchOCR](https://github.com/WenmuZhou/PytorchOCR)|基于Pytorch的OCR工具库，支持常用的文字检测和识别算法|766|2022-01-13|
|161|[tuplex/tuplex](https://github.com/tuplex/tuplex)|Tuplex is a parallel big data processing framework that runs data science pipelines written in Python at the speed of compiled code. Tuplex has similar Python APIs to Apache Spark or Dask, but rather  ...|746|2022-01-13|
|162|[linuxdeepin/deepin-boot-maker](https://github.com/linuxdeepin/deepin-boot-maker)|-|741|2021-12-15|
|163|[Qalculate/libqalculate](https://github.com/Qalculate/libqalculate)|Qalculate! library and CLI|738|2022-01-13|
|164|[daliansky/XiaoXinPro-13-hackintosh](https://github.com/daliansky/XiaoXinPro-13-hackintosh)|Lenovo XiaoXin Pro 13 2019 Hackintosh|719|2021-12-11|
|165|[SVF-tools/SVF](https://github.com/SVF-tools/SVF)|Static Value-Flow Analysis Framework for Source Code|701|2022-01-13|
|166|[OAID/AutoKernel](https://github.com/OAID/AutoKernel)|AutoKernel 是一个简单易用，低门槛的自动算子优化工具，提高深度学习算法部署效率。|692|2021-09-27|
|167|[universam1/iSpindel](https://github.com/universam1/iSpindel)|electronic Hydrometer|690|2022-01-10|
|168|[Rvn0xsy/Cooolis-ms](https://github.com/Rvn0xsy/Cooolis-ms)|Cooolis-ms是一个包含了Metasploit Payload Loader、Cobalt Strike External C2 Loader、Reflective DLL injection的代码执行工具，它的定位在于能够在静态查杀上规避一些我们将要执行且含有特征的代码，帮助红队人员更方便快捷的从Web容器环境切换到C2环境进一步进行工作。|689|2021-12-13|
|169|[mmc-maodun/Data-Structure-And-Algorithm](https://github.com/mmc-maodun/Data-Structure-And-Algorithm)|Data Structure And Algorithm（常用数据结构与算法C/C++实现）|677|2021-10-31|
|170|[downdemo/Cpp-Concurrency-in-Action-2ed](https://github.com/downdemo/Cpp-Concurrency-in-Action-2ed)|C++ Concurrency in Action 2ed 笔记：C++11/14/17/20 多线程，掌握操作系统原理，解锁并发编程技术|671|2022-01-08|
|171|[Tencent/DCache](https://github.com/Tencent/DCache)|A distributed in-memory NOSQL system based on TARS framework, support LRU algorithm and data persists on  back-end database. Users can easily deploy, publish, and scale services on the web interface.|669|2021-11-25|
|172|[troldal/OpenXLSX](https://github.com/troldal/OpenXLSX)|A C++ library for reading, writing, creating and modifying Microsoft Excel® (.xlsx) files.|660|2022-01-13|
|173|[qicosmos/ormpp](https://github.com/qicosmos/ormpp)|modern C++ ORM, C++17, support mysql, postgresql,sqlite|655|2021-10-03|
|174|[BeamMW/beam](https://github.com/BeamMW/beam)|Beam: Scalable Confidential Cryptocurrency. Leading the way to Confidential DeFi|652|2022-01-13|
|175|[SOUI2/soui](https://github.com/SOUI2/soui)|SOUI是目前为数不多的轻量级可快速开发window桌面程序开源DirectUI库.其前身为Duiengine,更早期则是源自于金山卫士开源版本UI库Bkwin.经过多年持续更新方得此库|646|2021-12-15|
|176|[openppl-public/ppl.nn](https://github.com/openppl-public/ppl.nn)|A primitive library for neural network|644|2022-01-13|
|177|[PHZ76/RtspServer](https://github.com/PHZ76/RtspServer)|RTSP Server , RTSP Pusher |638|2021-09-07|
|178|[J-D-K/JKSV](https://github.com/J-D-K/JKSV)|JK's Save Manager Switch Edition|630|2021-12-22|
|179|[Chaoses-Ib/IbEverythingExt](https://github.com/Chaoses-Ib/IbEverythingExt)|Everything 拼音搜索、快速选择扩展|622|2021-12-17|
|180|[anhkgg/SuperDllHijack](https://github.com/anhkgg/SuperDllHijack)|SuperDllHijack：A general DLL hijack technology, don't need to manually export the same function interface of the DLL, so easy! 一种通用Dll劫持技术，不再需要手工导出Dll的函数接口了|615|2021-11-10|
|181|[seetafaceengine/SeetaFace6](https://github.com/seetafaceengine/SeetaFace6)|SeetaFace 6: Newest open and free, full stack face recognization toolkit. |612|2021-11-29|
|182|[acidanthera/BrcmPatchRAM](https://github.com/acidanthera/BrcmPatchRAM)|-|606|2021-11-02|
|183|[AgoraIO/Basic-Video-Call](https://github.com/AgoraIO/Basic-Video-Call)|Sample app to join/leave a channel, mute/unmute, enable/disable the video, and switch between front/rear cameras.|598|2021-12-22|
|184|[iqiyi/libfiber](https://github.com/iqiyi/libfiber)|The high performance coroutine library for Linux/FreeBSD/MacOS/Windows, supporting select/poll/epoll/kqueue/iocp/windows GUI|596|2021-11-27|
|185|[tablacus/TablacusExplorer](https://github.com/tablacus/TablacusExplorer)|A tabbed file manager with Add-on support|590|2022-01-12|
|186|[HamletDuFromage/aio-switch-updater](https://github.com/HamletDuFromage/aio-switch-updater)|Update your CFW, sigpatches, cheat codes, firmwares and more directly from your Nintendo Switch!|588|2022-01-06|
|187|[wlgq2/uv-cpp](https://github.com/wlgq2/uv-cpp)|libuv wrapper in C++11 /libuv C++11网络库|588|2021-12-05|
|188|[shouxieai/tensorRT_Pro](https://github.com/shouxieai/tensorRT_Pro)|C++ library based on tensorrt integration|583|2021-12-15|
|189|[juzzlin/Heimer](https://github.com/juzzlin/Heimer)|Heimer is a simple cross-platform mind map, diagram, and note-taking tool written in Qt.|580|2022-01-11|
|190|[caozhiyi/CppNet](https://github.com/caozhiyi/CppNet)|Cross platform network library with C++11|572|2022-01-12|
|191|[lcatro/Source-and-Fuzzing](https://github.com/lcatro/Source-and-Fuzzing)|一些阅读源码和Fuzzing 的经验,涵盖黑盒与白盒测试..|571|2021-08-24|
|192|[mockingbirdnest/Principia](https://github.com/mockingbirdnest/Principia)|𝑛-Body and Extended Body Gravitation for Kerbal Space Program|564|2022-01-12|
|193|[githubhaohao/OpenGLCamera2](https://github.com/githubhaohao/OpenGLCamera2)|🔥 Android OpenGL Camera 2.0  实现 30 种滤镜和抖音特效|562|2021-10-11|
|194|[msnh2012/Msnhnet](https://github.com/msnh2012/Msnhnet)|🔥 (yolov3 yolov4 yolov5 unet ...)A mini pytorch inference framework which inspired from darknet.|558|2021-10-23|
|195|[eritpchy/Fingerprint-pay-magisk-wechat](https://github.com/eritpchy/Fingerprint-pay-magisk-wechat)|微信指纹支付 (Fingerprint pay for WeChat)|557|2021-09-01|
|196|[richenyunqi/CCF-CSP-and-PAT-solution](https://github.com/richenyunqi/CCF-CSP-and-PAT-solution)|CCF CSP和PAT考试题解（使用C++14语法）|556|2021-11-14|
|197|[NVIDIA-Merlin/HugeCTR](https://github.com/NVIDIA-Merlin/HugeCTR)|HugeCTR is a high efficiency GPU framework designed for Click-Through-Rate (CTR) estimating training|548|2022-01-11|
|198|[AntiMicroX/antimicrox](https://github.com/AntiMicroX/antimicrox)|Graphical program used to map keyboard buttons and mouse controls to a gamepad. Useful for playing games with no gamepad support.|545|2022-01-10|
|199|[HKUST-Aerial-Robotics/Teach-Repeat-Replan](https://github.com/HKUST-Aerial-Robotics/Teach-Repeat-Replan)|Teach-Repeat-Replan: A Complete and Robust System for Aggressive Flight in Complex Environments|542|2021-10-19|
|200|[BBuf/Image-processing-algorithm](https://github.com/BBuf/Image-processing-algorithm)|paper implement|542|2021-09-04|

<div align="center">
    <p><sub>↓ -- 感谢读者 -- ↓</sub></p>
    榜单持续更新，如有帮助请加星收藏，方便后续浏览，感谢你的支持！
</div>

<br/>

<div align="center"><a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a></div>
