<a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a>

# 中文增速榜 > 软件类 > Python
<sub>数据更新: 2022-01-14&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;温馨提示：中文项目泛指「文档母语为中文」OR「含有中文翻译」的项目，通常在项目的「readme/wiki/官网」可以找到</sub>

|#|Repository|Description|Stars|Average daily growth|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[Tencent/CodeAnalysis](https://github.com/Tencent/CodeAnalysis)|Static Code Analysis|912|54|2022-01-12|
|2|[xinntao/Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN)|Real-ESRGAN aims at developing Practical Algorithms for General Image Restoration.|8767|49|2022-01-13|
|3|[huggingface/transformers](https://github.com/huggingface/transformers)|🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.|56798|48|2022-01-13|
|4|[Textualize/rich](https://github.com/Textualize/rich)|Rich is a Python library for rich text and beautiful formatting in the terminal.|32245|41|2022-01-13|
|5|[PeterL1n/RobustVideoMatting](https://github.com/PeterL1n/RobustVideoMatting)|Robust Video Matting in PyTorch, TensorFlow, TensorFlow.js, ONNX, CoreML!|5073|37|2021-12-10|
|6|[PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR)|Awesome multilingual OCR toolkits based on PaddlePaddle (practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and de ...|18572|30|2022-01-13|
|7|[7eu7d7/genshin_auto_fish](https://github.com/7eu7d7/genshin_auto_fish)|基于深度强化学习的原神自动钓鱼AI|3016|24|2021-12-04|
|8|[ageitgey/face_recognition](https://github.com/ageitgey/face_recognition)|The world's simplest facial recognition api for Python and the command line|42816|24|2021-12-04|
|9|[taosdata/TDengine](https://github.com/taosdata/TDengine)|An open-source big data platform designed and optimized for the Internet of Things (IoT).|17560|19|2022-01-13|
|10|[testerSunshine/12306](https://github.com/testerSunshine/12306)|12306智能刷票，订票|30437|18|2021-08-25|
|11|[78778443/QingScan](https://github.com/78778443/QingScan)|一个漏洞扫描器粘合剂；支持 web扫描、系统扫描、子域名收集、目录扫描、主机扫描、主机发现、组件识别、URL爬虫、XRAY扫描、AWVS自动扫描、POC批量验证，SSH批量测试、vulmap。|694|18|2022-01-13|
|12|[open-mmlab/mmrazor](https://github.com/open-mmlab/mmrazor)|OpenMMLab Model Compression Toolbox and Benchmark.|399|17|2022-01-13|
|13|[Mas0nShi/typoraCracker](https://github.com/Mas0nShi/typoraCracker)|A extract & decryption and pack & encryption tools for typora.|724|16|2022-01-04|
|14|[3b1b/manim](https://github.com/3b1b/manim)|Animation engine for explanatory math videos|40177|16|2022-01-10|
|15|[521xueweihan/GitHub520](https://github.com/521xueweihan/GitHub520)|:kissing_heart: 让你“爱”上 GitHub，解决访问时图裂、加载慢的问题。（无需安装）|9230|15|2022-01-13|
|16|[open-mmlab/mmdetection](https://github.com/open-mmlab/mmdetection)|OpenMMLab Detection Toolbox and Benchmark|17966|14|2022-01-13|
|17|[lyhue1991/eat_tensorflow2_in_30_days](https://github.com/lyhue1991/eat_tensorflow2_in_30_days)|Tensorflow2.0 🍎🍊 is delicious, just eat it! 😋😋|9211|13|2022-01-07|
|18|[Kr1s77/awesome-python-login-model](https://github.com/Kr1s77/awesome-python-login-model)|😮python模拟登陆一些大型网站，还有一些简单的爬虫，希望对你们有所帮助❤️，如果喜欢记得给个star哦🌟|14009|13|2021-07-24|
|19|[pjialin/py12306](https://github.com/pjialin/py12306)|🚂 12306 购票助手，支持集群，多账号，多任务购票以及 Web 页面管理 |11658|11|2021-12-13|
|20|[tgbot-collection/YYeTsBot](https://github.com/tgbot-collection/YYeTsBot)|🎬 人人影视bot，完全对接人人影视全部无删减资源|8545|10|2022-01-11|
|21|[xfangfang/Macast](https://github.com/xfangfang/Macast)|Macast is a cross-platform application which using mpv as DLNA Media Renderer.|2345|10|2022-01-13|
|22|[lucidrains/DALLE-pytorch](https://github.com/lucidrains/DALLE-pytorch)|Implementation / replication of DALL-E, OpenAI's Text to Image Transformer, in Pytorch|3738|10|2022-01-12|
|23|[PaddlePaddle/PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN)|PaddlePaddle GAN library, including lots of interesting applications like First-Order motion transfer,  Wav2Lip, picture repair, image editing, photo2cartoon, image style transfer, GPEN, and so on.|5270|9|2022-01-13|
|24|[EssayKillerBrain/EssayKiller_V2](https://github.com/EssayKillerBrain/EssayKiller_V2)|基于开源GPT2.0的初代创作型人工智能   可扩展、可进化|4247|9|2022-01-05|
|25|[hankcs/HanLP](https://github.com/hankcs/HanLP)|Natural Language Processing for the next decade. Tokenization, Part-of-Speech Tagging, Named Entity Recognition, Syntactic & Semantic Dependency Parsing, Document Classification|24837|9|2022-01-12|
|26|[wangshub/wechat_jump_game](https://github.com/wangshub/wechat_jump_game)|微信《跳一跳》Python 辅助|13873|9|2022-01-13|
|27|[0x727/ShuiZe_0x727](https://github.com/0x727/ShuiZe_0x727)|信息收集自动化工具|1486|9|2022-01-05|
|28|[RangiLyu/nanodet](https://github.com/RangiLyu/nanodet)|NanoDet-Plus⚡Super fast and lightweight anchor-free object detection model. 🔥Only 980 KB(int8) / 1.8MB (fp16) and run 97FPS on cellphone🔥|3951|9|2022-01-11|
|29|[PaddlePaddle/Paddle](https://github.com/PaddlePaddle/Paddle)|PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice （『飞桨』核心框架，深度学习&机器学习高性能单机、分布式训练和跨平台部署）|17450|9|2022-01-13|
|30|[Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB](https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB)| 💎1MB lightweight face detection model  (1MB轻量级人脸检测模型)|6215|8|2021-11-25|
|31|[microsoft/nni](https://github.com/microsoft/nni)|An open source AutoML toolkit for automate machine learning lifecycle, including feature engineering, neural architecture search, model compression and hyper-parameter tuning.|10830|8|2022-01-13|
|32|[PaddlePaddle/PaddleDetection](https://github.com/PaddlePaddle/PaddleDetection)|Object Detection toolkit based on PaddlePaddle. It supports object detection, instance segmentation, multiple object tracking and real-time multi-person keypoint detection.|6151|8|2022-01-13|
|33|[martinet101/ElevenClock](https://github.com/martinet101/ElevenClock)|ElevenClock: Have a customizable clock on your Windows 11 displays|904|8|2022-01-12|
|34|[jhao104/proxy_pool](https://github.com/jhao104/proxy_pool)|Python爬虫代理IP池(proxy pool)|14136|8|2021-12-31|
|35|[open-mmlab/mmhuman3d](https://github.com/open-mmlab/mmhuman3d)|OpenMMLab 3D Human Parametric Model Toolbox and Benchmark|380|8|2022-01-13|
|36|[PaddlePaddle/PaddleNLP](https://github.com/PaddlePaddle/PaddleNLP)|Easy-to-use and Fast NLP library with awesome model zoo,  supporting wide-range of NLP tasks from research to industrial applications.|2682|8|2022-01-13|
|37|[WZMIAOMIAO/deep-learning-for-image-processing](https://github.com/WZMIAOMIAO/deep-learning-for-image-processing)|deep learning for image processing including classification and object-detection etc.|6160|8|2022-01-13|
|38|[Jack-Cherish/python-spider](https://github.com/Jack-Cherish/python-spider)|:rainbow:Python3网络爬虫实战：淘宝、京东、网易云、B站、12306、抖音、笔趣阁、漫画小说下载、音乐电影下载等|14341|8|2021-12-13|
|39|[luminoleon/epicgames-claimer](https://github.com/luminoleon/epicgames-claimer)|自动领取Epic游戏商城每周免费游戏。|974|8|2022-01-13|
|40|[fxsjy/jieba](https://github.com/fxsjy/jieba)|结巴中文分词|27714|8|2021-07-25|
|41|[open-mmlab/mmocr](https://github.com/open-mmlab/mmocr)|OpenMMLab Text Detection, Recognition and Understanding Toolbox|2034|7|2022-01-13|
|42|[ymcui/Chinese-BERT-wwm](https://github.com/ymcui/Chinese-BERT-wwm)|Pre-Training with Whole Word Masking for Chinese BERT（中文BERT-wwm系列模型）|6450|7|2022-01-10|
|43|[open-mmlab/mmflow](https://github.com/open-mmlab/mmflow)|OpenMMLab optical flow toolbox and benchmark|391|7|2022-01-12|
|44|[PaddlePaddle/PaddleHub](https://github.com/PaddlePaddle/PaddleHub)|Awesome pre-trained models toolkit based on PaddlePaddle.(300+ models including Image, Text, Audio and Video with Easy Inference & Serving deployment)|7410|7|2022-01-07|
|45|[pyecharts/pyecharts](https://github.com/pyecharts/pyecharts)|🎨 Python Echarts Plotting Library|11901|7|2022-01-13|
|46|[vnpy/vnpy](https://github.com/vnpy/vnpy)|基于Python的开源量化交易平台开发框架|17351|7|2021-12-31|
|47|[Jack-Cherish/quantitative](https://github.com/Jack-Cherish/quantitative)|量化交易：python3|730|6|2021-12-28|
|48|[sqlmapproject/sqlmap](https://github.com/sqlmapproject/sqlmap)|Automatic SQL injection and database takeover tool|22138|6|2022-01-11|
|49|[wangshub/Douyin-Bot](https://github.com/wangshub/Douyin-Bot)|😍 Python 抖音机器人，论如何在抖音上找到漂亮小姐姐？ |7685|6|2022-01-13|
|50|[biancangming/wtv](https://github.com/biancangming/wtv)|解决电脑、手机看电视直播的苦恼，收集各种直播源，电视直播网站|4860|6|2021-12-26|
|51|[dmlc/dgl](https://github.com/dmlc/dgl)|Python package built to ease deep learning on graph, on top of existing DL frameworks.|8777|6|2022-01-13|
|52|[sml2h3/ddddocr](https://github.com/sml2h3/ddddocr)|带带弟弟 通用验证码识别OCR pypi版|1045|6|2022-01-10|
|53|[Mashiro2000/HeyTapTask](https://github.com/Mashiro2000/HeyTapTask)|适配青龙面板/云函数/本地运行的欢太商城脚本|741|6|2021-12-03|
|54|[xingyizhou/CenterNet](https://github.com/xingyizhou/CenterNet)|Object detection, 3D detection, and pose estimation using center point detection: |6145|6|2021-11-11|
|55|[hustvl/YOLOP](https://github.com/hustvl/YOLOP)|You Only Look Once for Panopitic Driving Perception.（https://arxiv.org/abs/2108.11250）|836|6|2021-12-14|
|56|[jumpserver/jumpserver](https://github.com/jumpserver/jumpserver)|JumpServer 是全球首款开源的堡垒机，是符合 4A 的专业运维安全审计系统。|17729|6|2022-01-13|
|57|[wbt5/real-url](https://github.com/wbt5/real-url)|获取斗鱼&虎牙&哔哩哔哩&抖音&快手等 58 个直播平台的真实流媒体地址(直播源)和弹幕，直播源可在 PotPlayer、flv.js 等播放器中播放。|3874|5|2021-12-30|
|58|[PaddlePaddle/PaddleX](https://github.com/PaddlePaddle/PaddleX)|PaddlePaddle End-to-End Development Toolkit（『飞桨』深度学习全流程开发工具）|3448|5|2022-01-10|
|59|[hzwer/arXiv2020-RIFE](https://github.com/hzwer/arXiv2020-RIFE)|RIFE: Real-Time Intermediate Flow Estimation for Video Frame Interpolation|2217|5|2022-01-12|
|60|[Zy143L/wskey](https://github.com/Zy143L/wskey)|wskey|668|5|2022-01-13|
|61|[xiaolai/regular-investing-in-box](https://github.com/xiaolai/regular-investing-in-box)|定投改变命运 —— 让时间陪你慢慢变富 https://onregularinvesting.com|4731|5|2022-01-13|
|62|[bojone/bert4keras](https://github.com/bojone/bert4keras)|keras implement of transformers for humans|4053|5|2021-12-25|
|63|[FengQuanLi/ResnetGPT](https://github.com/FengQuanLi/ResnetGPT)|用Resnet101+GPT搭建一个玩王者荣耀的AI|1839|5|2021-08-08|
|64|[Morizeyao/GPT2-Chinese](https://github.com/Morizeyao/GPT2-Chinese)|Chinese version of GPT2 training code, using BERT tokenizer.|4659|5|2021-12-16|
|65|[akfamily/akshare](https://github.com/akfamily/akshare)|AKShare is an elegant and simple financial data interface library for Python, built for human beings! 开源财经数据接口库|4448|5|2022-01-13|
|66|[PaddlePaddle/PaddleClas](https://github.com/PaddlePaddle/PaddleClas)|A treasure chest for visual recognition powered by PaddlePaddle|3225|5|2022-01-13|
|67|[rwv/chinese-dos-games](https://github.com/rwv/chinese-dos-games)|🎮 Chinese DOS games collections.|6565|5|2021-12-30|
|68|[microsoft/unilm](https://github.com/microsoft/unilm)|Large-scale Self-supervised Pre-training Across Tasks, Languages, and Modalities|4356|5|2022-01-13|
|69|[open-mmlab/mmsegmentation](https://github.com/open-mmlab/mmsegmentation)|OpenMMLab Semantic Segmentation Toolbox and Benchmark.|2996|5|2022-01-13|
|70|[stypr/clubhouse-py](https://github.com/stypr/clubhouse-py)|Clubhouse API written in Python. Standalone client included. For reference and education purposes only.|1690|5|2021-07-28|
|71|[OlafenwaMoses/ImageAI](https://github.com/OlafenwaMoses/ImageAI)|A python library built to empower developers to build applications and systems  with self-contained Computer Vision capabilities|6786|5|2022-01-13|
|72|[minivision-ai/photo2cartoon](https://github.com/minivision-ai/photo2cartoon)|人像卡通化探索项目 (photo-to-cartoon translation project)|2885|5|2021-12-02|
|73|[lyhue1991/eat_pytorch_in_20_days](https://github.com/lyhue1991/eat_pytorch_in_20_days)|Pytorch🍊🍉   is delicious, just eat it! 😋😋|2844|5|2021-11-20|
|74|[lancopku/pkuseg-python](https://github.com/lancopku/pkuseg-python)|pkuseg多领域中文分词工具; The pkuseg toolkit for multi-domain Chinese word segmentation|5723|5|2021-10-19|
|75|[kornia/kornia](https://github.com/kornia/kornia)|Open Source Differentiable Computer Vision Library|5704|5|2022-01-12|
|76|[BR-IDL/PaddleViT](https://github.com/BR-IDL/PaddleViT)|:robot: PaddleViT: State-of-the-art Visual Transformer and MLP Models for PaddlePaddle 2.0+|730|5|2022-01-11|
|77|[meolu/walle-web](https://github.com/meolu/walle-web)|walle - 瓦力 Devops开源项目代码部署平台|11314|5|2022-01-06|
|78|[tianqiraf/DouZero_For_HappyDouDiZhu](https://github.com/tianqiraf/DouZero_For_HappyDouDiZhu)|基于DouZero定制AI实战欢乐斗地主|723|4|2021-07-22|
|79|[Oshino29/ngaArchive](https://github.com/Oshino29/ngaArchive)|nga论坛帖子的存档|454|4|2021-10-10|
|80|[luyishisi/Anti-Anti-Spider](https://github.com/luyishisi/Anti-Anti-Spider)|越来越多的网站具有反爬虫特性，有的用图片隐藏关键数据，有的使用反人类的验证码，建立反反爬虫的代码仓库，通过与不同特性的网站做斗争（无恶意）提高技术。（欢迎提交难以采集的网站）（因工作原因，项目暂停） |6923|4|2021-10-17|
|81|[x-hw/amazing-qr](https://github.com/x-hw/amazing-qr)|💮 amazing QRCode generator in Python (supporting animated gif) - Python amazing 二维码生成器（支持 gif 动态图片二维码）|8638|4|2022-01-13|
|82|[curtinlv/JD-Script](https://github.com/curtinlv/JD-Script)|仅供参考。|998|4|2022-01-12|
|83|[open-mmlab/mmtracking](https://github.com/open-mmlab/mmtracking)|OpenMMLab Video Perception Toolbox. It supports Video Object Detection (VID), Multiple Object Tracking (MOT), Single Object Tracking (SOT), Video Instance Segmentation (VIS) with a unified framework.|1976|4|2022-01-13|
|84|[brightmart/text_classification](https://github.com/brightmart/text_classification)|all kinds of text classification models and more with deep learning|7201|4|2021-11-02|
|85|[PaddlePaddle/PaddleSeg](https://github.com/PaddlePaddle/PaddleSeg)|Easy-to-use image segmentation library with awesome pre-trained model zoo, supporting wide-range of practical tasks in Semantic Segmentation, Interactive Segmentation, Panoptic Segmentation, Image Mat ...|3542|4|2022-01-13|
|86|[TheKingOfDuck/fuzzDicts](https://github.com/TheKingOfDuck/fuzzDicts)|Web Pentesting Fuzz 字典,一个就够了。|4166|4|2021-12-11|
|87|[DA-southampton/TRM_tutorial](https://github.com/DA-southampton/TRM_tutorial)|Transformer在CV和NLP领域的变体模型的从零解读：Transformer；VIT；Swin Transformer|217|4|2021-12-05|
|88|[TophantTechnology/ARL](https://github.com/TophantTechnology/ARL)|ARL(Asset Reconnaissance Lighthouse)资产侦察灯塔系统旨在快速侦察与目标关联的互联网资产，构建基础资产信息库。 协助甲方安全团队或者渗透测试人员有效侦察和检索资产，发现存在的薄弱点和攻击面。|2209|4|2021-12-24|
|89|[zhzyker/vulmap](https://github.com/zhzyker/vulmap)|Vulmap 是一款 web 漏洞扫描和验证工具, 可对 webapps 进行漏洞扫描, 并且具备漏洞验证功能|2048|4|2021-11-15|
|90|[shmilylty/OneForAll](https://github.com/shmilylty/OneForAll)|OneForAll是一款功能强大的子域收集工具|4281|4|2021-12-30|
|91|[CollegesChat/university-information](https://github.com/CollegesChat/university-information)|收集全国各高校招生时不会写明，却会实实在在影响大学生活质量的要求与细节|766|4|2021-08-25|
|92|[celery/celery](https://github.com/celery/celery)|Distributed Task Queue (development branch)|18504|4|2022-01-13|
|93|[TensorSpeech/TensorFlowTTS](https://github.com/TensorSpeech/TensorFlowTTS)|:stuck_out_tongue_closed_eyes: TensorFlowTTS: Real-Time State-of-the-art Speech Synthesis for Tensorflow 2 (supported including English, French, Korean, Chinese, German and Easy to adapt for other lan ...|2437|4|2021-11-29|
|94|[yuanxiaosc/DeepNude-an-Image-to-Image-technology](https://github.com/yuanxiaosc/DeepNude-an-Image-to-Image-technology)|DeepNude's algorithm and general image generation theory and practice research, including pix2pix, CycleGAN, UGATIT, DCGAN, SinGAN, ALAE, mGANprior, StarGAN-v2 and VAE models (TensorFlow2 implementati ...|4049|4|2021-10-02|
|95|[yoshiko2/Movie_Data_Capture](https://github.com/yoshiko2/Movie_Data_Capture)|本地电影刮削与整理一体化解决方案|3603|4|2022-01-06|
|96|[open-mmlab/mmdetection3d](https://github.com/open-mmlab/mmdetection3d)|OpenMMLab's next-generation platform for general 3D object detection.|1969|4|2022-01-12|
|97|[wb14123/seq2seq-couplet](https://github.com/wb14123/seq2seq-couplet)|Play couplet with seq2seq model. 用深度学习对对联。|5175|4|2021-10-15|
|98|[alibaba/taobao-iphone-device](https://github.com/alibaba/taobao-iphone-device)| tidevice can be used to communicate with iPhone device|1308|4|2021-12-31|
|99|[ufoym/deepo](https://github.com/ufoym/deepo)|Setup and customize deep learning environment in seconds.|6161|4|2022-01-04|
|100|[Jrohy/multi-v2ray](https://github.com/Jrohy/multi-v2ray)|v2ray/xray多用户管理部署程序|5429|4|2021-11-23|
|101|[PaddlePaddle/models](https://github.com/PaddlePaddle/models)|Pre-trained and Reproduced Deep Learning Models （『飞桨』官方模型库，包含多种学术前沿和工业场景验证的深度学习模型）|6338|4|2022-01-13|
|102|[PaddlePaddle/ERNIE](https://github.com/PaddlePaddle/ERNIE)|Official implementations for various pre-training models of ERNIE-family, covering topics of Language Understanding & Generation, Multimodal Understanding & Generation, and beyond.|4694|4|2021-12-27|
|103|[ClassmateLin/scripts](https://github.com/ClassmateLin/scripts)|jd|595|3|2022-01-10|
|104|[thu-ml/tianshou](https://github.com/thu-ml/tianshou)|An elegant PyTorch deep reinforcement learning library.|4204|3|2022-01-13|
|105|[YunYang1994/tensorflow-yolov3](https://github.com/YunYang1994/tensorflow-yolov3)|🔥 TensorFlow Code for technical report: "YOLOv3: An Incremental Improvement"|3507|3|2022-01-13|
|106|[open-mmlab/mmcv](https://github.com/open-mmlab/mmcv)|OpenMMLab Computer Vision Foundation|3378|3|2022-01-13|
|107|[tychxn/jd-assistant](https://github.com/tychxn/jd-assistant)|京东抢购助手：包含登录，查询商品库存/价格，添加/清空购物车，抢购商品(下单)，查询订单等功能|4363|3|2021-09-06|
|108|[ghealer/GUI_Tools](https://github.com/ghealer/GUI_Tools)|一个由各种图形化渗透工具组成的工具集|452|3|2022-01-10|
|109|[ha0y/xiaomi_miot_raw](https://github.com/ha0y/xiaomi_miot_raw)|All-in-one & Easy-to-use. Integrate all your Xiaomi Smart Home - with a single integration and NO YAML files - into Home Assistant.|1055|3|2022-01-04|
|110|[SpiderClub/haipproxy](https://github.com/SpiderClub/haipproxy)|:sparkling_heart: High available distributed ip proxy pool, powerd by Scrapy and Redis|5017|3|2021-10-06|
|111|[tensorlayer/TensorLayer](https://github.com/tensorlayer/TensorLayer)|Deep Learning and Reinforcement Learning Library for Scientists and Engineers 🔥|6810|3|2021-12-03|
|112|[wzhe06/SparrowRecSys](https://github.com/wzhe06/SparrowRecSys)|A Deep Learning Recommender System|1631|3|2022-01-13|
|113|[jerry3747/taobao_seckill](https://github.com/jerry3747/taobao_seckill)|淘宝、天猫半价抢购，抢电视、抢茅台，干死黄牛党|1225|3|2021-11-09|
|114|[TsinghuaAI/CPM-1-Generate](https://github.com/TsinghuaAI/CPM-1-Generate)|Chinese Pre-Trained Language Models (CPM-LM) Version-I|1176|3|2021-09-26|
|115|[jiangxufeng/v2rayL](https://github.com/jiangxufeng/v2rayL)|v2ray linux GUI客户端，支持订阅、vemss、ss等协议，自动更新订阅、检查版本更新|2752|3|2022-01-13|
|116|[JustMachiavelli/javsdt](https://github.com/JustMachiavelli/javsdt)|影片信息整理工具，抓取元数据nfo，自定义重命名文件(夹)，下载fanart裁剪poster，为emby、kodi、极影派铺路。|2726|3|2022-01-13|
|117|[aaPanel/BaoTa](https://github.com/aaPanel/BaoTa)|宝塔Linux面板 - 简单好用的服务器运维面板|3214|3|2022-01-13|
|118|[darknessomi/musicbox](https://github.com/darknessomi/musicbox)|网易云音乐命令行版本|9164|3|2022-01-08|
|119|[chineseocr/chineseocr](https://github.com/chineseocr/chineseocr)|yolo3+ocr|4598|3|2021-11-02|
|120|[CharlesPikachu/Games](https://github.com/CharlesPikachu/Games)|Games: Create interesting games by pure python.|3527|3|2022-01-11|
|121|[microsoft/Graphormer](https://github.com/microsoft/Graphormer)|Graphormer is a deep learning package that allows researchers and developers to train custom models for molecule modeling tasks. It aims to accelerate the research and application in AI for molecule s ...|726|3|2022-01-07|
|122|[hhyo/Archery](https://github.com/hhyo/Archery)|SQL 审核查询平台|3347|3|2022-01-13|
|123|[prompt-toolkit/python-prompt-toolkit](https://github.com/prompt-toolkit/python-prompt-toolkit)|Library for building powerful interactive command line applications in Python|7483|3|2022-01-10|
|124|[0xHJK/music-dl](https://github.com/0xHJK/music-dl)|search and download music 从网易云音乐、QQ音乐、酷狗音乐、百度音乐、虾米音乐、咪咕音乐等搜索和下载歌曲|2818|3|2021-08-14|
|125|[Jittor/jittor](https://github.com/Jittor/jittor)|Jittor is a high-performance deep learning framework based on JIT compiling and meta-operators.|2326|3|2022-01-12|
|126|[yihong0618/running_page](https://github.com/yihong0618/running_page)|Make your own running home page|1571|3|2022-01-11|
|127|[QUANTAXIS/QUANTAXIS](https://github.com/QUANTAXIS/QUANTAXIS)|QUANTAXIS 支持任务调度 分布式部署的 股票/期货/期权/港股/虚拟货币  数据/回测/模拟/交易/可视化/多账户 纯本地量化解决方案|6218|3|2022-01-04|
|128|[ventusff/neurecon](https://github.com/ventusff/neurecon)|Multi-view 3D reconstruction using neural rendering. Unofficial implementation of UNISURF, VolSDF, NeuS and more.|387|3|2021-09-14|
|129|[zhanyong-wan/dongbei](https://github.com/zhanyong-wan/dongbei)|东北方言编程语言|1861|3|2021-10-07|
|130|[shidenggui/easytrader](https://github.com/shidenggui/easytrader)|提供同花顺客户端/国金/华泰客户端/雪球的基金、股票自动程序化交易以及自动打新，支持跟踪 joinquant /ricequant 模拟交易 和 实盘雪球组合, 量化交易组件|6254|3|2021-12-13|
|131|[chatopera/Synonyms](https://github.com/chatopera/Synonyms)|:herb: 中文近义词：聊天机器人，智能问答工具包|4061|3|2021-09-14|
|132|[yihong0618/GitHubPoster](https://github.com/yihong0618/GitHubPoster)|Make everything a GitHub svg poster and Skyline!|730|3|2022-01-13|
|133|[man-group/dtale](https://github.com/man-group/dtale)|Visualizer for pandas data structures|2912|3|2022-01-13|
|134|[zq1997/deepin-wine](https://github.com/zq1997/deepin-wine)|【deepin源移植】Debian/Ubuntu上最快的QQ/微信安装方式|2920|3|2021-11-25|
|135|[ivan-bilan/The-NLP-Pandect](https://github.com/ivan-bilan/The-NLP-Pandect)|A comprehensive reference for all topics related to Natural Language Processing|1640|3|2022-01-07|
|136|[xinntao/ESRGAN](https://github.com/xinntao/ESRGAN)|ECCV18 Workshops - Enhanced SRGAN. Champion PIRM Challenge on Perceptual Super-Resolution. The training codes are in BasicSR.|3916|3|2021-08-25|
|137|[open-mmlab/mmaction2](https://github.com/open-mmlab/mmaction2)|OpenMMLab's Next Generation Video Understanding Toolbox and Benchmark|1620|3|2022-01-13|
|138|[huawei-noah/Pretrained-Language-Model](https://github.com/huawei-noah/Pretrained-Language-Model)|Pretrained language model and its related optimization techniques developed by Huawei Noah's Ark Lab.|2075|3|2022-01-05|
|139|[CLUEbenchmark/CLUE](https://github.com/CLUEbenchmark/CLUE)|中文语言理解测评基准 Chinese Language Understanding Evaluation Benchmark: datasets, baselines, pre-trained models, corpus and leaderboard  |2470|3|2022-01-07|
|140|[zpoint/CPython-Internals](https://github.com/zpoint/CPython-Internals)|Dive into CPython internals, trying to illustrate every detail of CPython implementation|3149|3|2021-12-20|
|141|[hunshcn/gh-proxy](https://github.com/hunshcn/gh-proxy)|github release、archive以及项目文件的加速项目|2198|3|2022-01-04|
|142|[zhimingshenjun/DD_Monitor](https://github.com/zhimingshenjun/DD_Monitor)|DD监控室第一版|1005|3|2021-10-14|
|143|[PiotrMachowski/Xiaomi-cloud-tokens-extractor](https://github.com/PiotrMachowski/Xiaomi-cloud-tokens-extractor)|This tool/script retrieves tokens for all devices connected to Xiaomi cloud and encryption keys for BLE devices.|1503|3|2022-01-04|
|144|[wzpan/wukong-robot](https://github.com/wzpan/wukong-robot)|🤖 wukong-robot 是一个简单、灵活、优雅的中文语音对话机器人/智能音箱项目，还可能是首个支持脑机交互的开源智能音箱项目。|3146|3|2021-12-05|
|145|[GeneralNewsExtractor/GeneralNewsExtractor](https://github.com/GeneralNewsExtractor/GeneralNewsExtractor)| 新闻网页正文通用抽取器 Beta 版.|2332|3|2021-10-11|
|146|[BlankerL/DXY-COVID-19-Data](https://github.com/BlankerL/DXY-COVID-19-Data)|2019新型冠状病毒疫情时间序列数据仓库   COVID-19/2019-nCoV Infection Time Series Data Warehouse|2026|3|2022-01-13|
|147|[nl8590687/ASRT_SpeechRecognition](https://github.com/nl8590687/ASRT_SpeechRecognition)|A Deep-Learning-Based Chinese Speech Recognition System 基于深度学习的中文语音识别系统|5015|3|2022-01-13|
|148|[open-mmlab/mmpose](https://github.com/open-mmlab/mmpose)|OpenMMLab Pose Estimation Toolbox and Benchmark.|1537|3|2022-01-13|
|149|[fbdesignpro/sweetviz](https://github.com/fbdesignpro/sweetviz)|Visualize and compare datasets, target values and associations, with one line of code.|1881|3|2021-10-01|
|150|[ctf-wiki/ctf-wiki](https://github.com/ctf-wiki/ctf-wiki)|Come and join us, we need you!|5362|3|2021-11-29|
|151|[knownsec/Kunyu](https://github.com/knownsec/Kunyu)|Kunyu, more efficient corporate asset collection|533|3|2022-01-04|
|152|[StepfenShawn/Cantonese](https://github.com/StepfenShawn/Cantonese)|粤语编程语言.The Cantonese programming language.|1063|3|2022-01-09|
|153|[ASoulCnki/ASoulCnki](https://github.com/ASoulCnki/ASoulCnki)|ASoul评论区小作文 枝网查重系统 爬虫部分|546|3|2021-12-03|
|154|[open-mmlab/mmediting](https://github.com/open-mmlab/mmediting)|OpenMMLab Image and Video Editing Toolbox|2678|3|2022-01-13|
|155|[microsoft/AI-System](https://github.com/microsoft/AI-System)|System for AI Education Resource.|1381|3|2022-01-13|
|156|[pythonstock/stock](https://github.com/pythonstock/stock)|stock，股票系统。使用python进行开发。|4915|3|2022-01-12|
|157|[hummingbot/hummingbot](https://github.com/hummingbot/hummingbot)|Hummingbot is open source software that helps you build trading bots that run on any exchange or blockchain|3526|3|2022-01-13|
|158|[opendevops-cn/opendevops](https://github.com/opendevops-cn/opendevops)|CODO是一款为用户提供企业多混合云、一站式DevOps、自动化运维、完全开源的云管理平台、自动化运维平台|3021|3|2021-10-24|
|159|[enpeizhao/CVprojects](https://github.com/enpeizhao/CVprojects)|computer vision projects    计算机视觉等好玩的AI项目|149|3|2021-12-13|
|160|[RUCAIBox/RecBole](https://github.com/RUCAIBox/RecBole)|A unified, comprehensive and efficient recommendation library|1562|3|2022-01-10|
|161|[daquexian/onnx-simplifier](https://github.com/daquexian/onnx-simplifier)|Simplify your onnx model|1825|2|2021-12-31|
|162|[codemayq/chinese_chatbot_corpus](https://github.com/codemayq/chinese_chatbot_corpus)|中文公开聊天语料库|2709|2|2021-08-04|
|163|[garrettj403/SciencePlots](https://github.com/garrettj403/SciencePlots)|Matplotlib styles for scientific plotting|3009|2|2021-11-16|
|164|[DLLXW/data-science-competition](https://github.com/DLLXW/data-science-competition)|该仓库用于记录作者本人参加的各大数据科学竞赛的获奖方案源码以及一些新比赛的原创baseline. 主要涵盖：kaggle, 阿里天池，华为云大赛校园赛，百度aistudio，和鲸社区，datafountain等|873|2|2022-01-11|
|165|[pdm-project/pdm](https://github.com/pdm-project/pdm)|A modern Python package manager with PEP 582 support.|1788|2|2022-01-12|
|166|[EASY233/Finger](https://github.com/EASY233/Finger)|一款红队在大量的资产中存活探测与重点攻击系统指纹探测工具|498|2|2022-01-11|
|167|[datamllab/rlcard](https://github.com/datamllab/rlcard)|Reinforcement Learning / AI Bots in Card (Poker) Games - Blackjack, Leduc, Texas, DouDizhu, Mahjong, UNO.|1563|2|2021-12-16|
|168|[shibing624/pycorrector](https://github.com/shibing624/pycorrector)|pycorrector is a toolkit for text error correction. 文本纠错，Kenlm，Seq2Seq_Attention，BERT，MacBERT，ELECTRA，ERNIE，Transformer等模型实现，开箱即用。|2927|2|2022-01-06|
|169|[CTF-MissFeng/bayonet](https://github.com/CTF-MissFeng/bayonet)|bayonet是一款src资产管理系统，从子域名、端口服务、漏洞、爬虫等一体化的资产管理系统|1092|2|2022-01-13|
|170|[P1-Team/AlliN](https://github.com/P1-Team/AlliN)|A flexible scanner|498|2|2021-12-17|
|171|[Delta-ML/delta](https://github.com/Delta-ML/delta)|DELTA is a deep learning based natural language and speech processing platform.|1483|2|2021-11-08|
|172|[Python3WebSpider/ProxyPool](https://github.com/Python3WebSpider/ProxyPool)|An Efficient ProxyPool with Getter, Tester and Server|3124|2|2022-01-02|
|173|[HIT-SCIR/plm-nlp-code](https://github.com/HIT-SCIR/plm-nlp-code)|-|292|2|2021-12-15|
|174|[newpanjing/simpleui](https://github.com/newpanjing/simpleui)|A modern theme based on vue+element-ui for django admin.一款基于vue+element-ui的django admin现代化主题。全球20000+网站都在使用！喜欢可以点个star✨|2438|2|2021-12-13|
|175|[Tencent/Lichee](https://github.com/Tencent/Lichee)|一个多模态内容理解算法框架，其中包含数据处理、预训练模型、常见模型以及模型加速等模块。|176|2|2021-10-26|
|176|[ChineseGLUE/ChineseGLUE](https://github.com/ChineseGLUE/ChineseGLUE)|Language Understanding Evaluation benchmark for Chinese: datasets, baselines, pre-trained models,corpus and leaderboard|1556|2|2021-12-01|
|177|[AlexxIT/XiaomiGateway3](https://github.com/AlexxIT/XiaomiGateway3)|Control Zigbee, BLE and Mesh devices from Home Assistant with Xiaomi Gateway 3 (ZNDMWG03LM) on original firmware|1208|2|2022-01-13|
|178|[lobbyboy-ssh/lobbyboy](https://github.com/lobbyboy-ssh/lobbyboy)|A lobby boy will create a VPS server when you need one, and destroy it after using it.|198|2|2021-12-12|
|179|[PaddlePaddle/PARL](https://github.com/PaddlePaddle/PARL)|A high-performance distributed training framework for Reinforcement Learning |2383|2|2022-01-07|
|180|[MoyuScript/bilibili-api](https://github.com/MoyuScript/bilibili-api)|哔哩哔哩的API调用模块|1458|2|2022-01-09|
|181|[hukaixuan19970627/yolov5_obb](https://github.com/hukaixuan19970627/yolov5_obb)|yolov5 + csl_label.(Oriented Object Detection)（Rotation Detection）（Rotated BBox）基于yolov5的旋转目标检测|483|2|2022-01-13|
|182|[DataCanvasIO/Hypernets](https://github.com/DataCanvasIO/Hypernets)|A General Automated Machine Learning framework to simplify the development of End-to-end AutoML toolkits in specific domains.|1036|2|2022-01-13|
|183|[dbiir/UER-py](https://github.com/dbiir/UER-py)|Open Source Pre-training Model Framework in PyTorch & Pre-trained Model Zoo|1868|2|2022-01-11|
|184|[Tramac/awesome-semantic-segmentation-pytorch](https://github.com/Tramac/awesome-semantic-segmentation-pytorch)|Semantic Segmentation on PyTorch (include FCN, PSPNet, Deeplabv3, Deeplabv3+, DANet, DenseASPP, BiSeNet, EncNet, DUNet, ICNet, ENet, OCNet, CCNet, PSANet, CGNet, ESPNet, LEDNet, DFANet)|2050|2|2021-09-15|
|185|[dataabc/weibo-crawler](https://github.com/dataabc/weibo-crawler)|新浪微博爬虫，用python爬取新浪微博数据，并下载微博图片和微博视频|1523|2|2021-12-29|
|186|[parzulpan/reallive](https://github.com/parzulpan/reallive)|A cross-platform network media aggregation application.|1010|2|2021-10-12|
|187|[Dman95/SASM](https://github.com/Dman95/SASM)|SASM - simple crossplatform IDE for NASM, MASM, GAS and FASM assembly languages|5554|2|2022-01-03|
|188|[Uberi/speech_recognition](https://github.com/Uberi/speech_recognition)|Speech recognition module for Python, supporting several engines and APIs, online and offline.|6025|2|2021-12-14|
|189|[ricequant/rqalpha](https://github.com/ricequant/rqalpha)|A extendable, replaceable Python algorithmic backtest && trading framework supporting multiple securities|4449|2|2022-01-13|
|190|[Azure/Stormspotter](https://github.com/Azure/Stormspotter)|Azure Red Team tool for graphing Azure and Azure Active Directory objects|953|2|2021-12-10|
|191|[liangliangyy/DjangoBlog](https://github.com/liangliangyy/DjangoBlog)|🍺基于Django的博客系统|4299|2|2022-01-13|
|192|[AntonVanke/JDBrandMember](https://github.com/AntonVanke/JDBrandMember)|京东自动入会获取京豆|479|2|2021-11-15|
|193|[yhy0/github-cve-monitor](https://github.com/yhy0/github-cve-monitor)|实时监控github上新增的cve和安全工具更新，多渠道推送通知|486|2|2022-01-11|
|194|[imWildCat/scylla](https://github.com/imWildCat/scylla)|Intelligent proxy pool for Humans™ (Maintainer needed)|3418|2|2022-01-13|
|195|[PaddlePaddle/Research](https://github.com/PaddlePaddle/Research)|novel deep learning research works with PaddlePaddle|1134|2|2022-01-13|
|196|[sunfkny/genshin-gacha-export](https://github.com/sunfkny/genshin-gacha-export)|原神抽卡记录导出|640|2|2022-01-12|
|197|[wepe/MachineLearning](https://github.com/wepe/MachineLearning)|Basic Machine Learning and Deep Learning|4279|2|2021-10-04|
|198|[ZainCheung/netease-cloud](https://github.com/ZainCheung/netease-cloud)|网易云音乐全自动每日打卡300首歌升级账号等级，支持微信提醒，支持无服务器云函数部署|1238|2|2022-01-08|
|199|[yym6472/ConSERT](https://github.com/yym6472/ConSERT)|Code for our ACL 2021 paper - ConSERT: A Contrastive Framework for Self-Supervised Sentence Representation Transfer|348|2|2021-12-10|
|200|[blackboxo/CleanMyWechat](https://github.com/blackboxo/CleanMyWechat)|自动删除 PC 端微信缓存数据，包括从所有聊天中自动下载的大量文件、视频、图片等数据内容，解放你的空间。|1153|2|2021-11-18|

<div align="center">
    <p><sub>↓ -- 感谢读者 -- ↓</sub></p>
    榜单持续更新，如有帮助请加星收藏，方便后续浏览，感谢你的支持！
</div>

<br/>

<div align="center"><a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a></div>
