<a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a>

# 中文增速榜 > 软件类 > C++
<sub>数据更新: 2022-01-14&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;温馨提示：中文项目泛指「文档母语为中文」OR「含有中文翻译」的项目，通常在项目的「readme/wiki/官网」可以找到</sub>

|#|Repository|Description|Stars|Average daily growth|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[EdVince/QRCode-NCNN](https://github.com/EdVince/QRCode-NCNN)|QRCode(from WeChat) implement in ncnn⚡二维码检测&解码⚡ncnn⚡|78|39|2022-01-13|
|2|[electron/electron](https://github.com/electron/electron)|:electron: Build cross-platform desktop apps with JavaScript, HTML, and CSS|99709|31|2022-01-13|
|3|[open-mmlab/mmdeploy](https://github.com/open-mmlab/mmdeploy)|OpenMMLab Model Deployment Framework|519|25|2022-01-13|
|4|[oceanbase/oceanbase](https://github.com/oceanbase/oceanbase)|OceanBase is an enterprise distributed relational database with high availability, high performance, horizontal scalability, and compatibility with SQL standards.|4062|18|2022-01-11|
|5|[sogou/workflow](https://github.com/sogou/workflow)|C++ Parallel Computing and Asynchronous Networking Engine|6931|13|2022-01-13|
|6|[Tencent/MMKV](https://github.com/Tencent/MMKV)|An efficient, small mobile key-value storage framework developed by WeChat. Works on Android, iOS, macOS, Windows, and POSIX.|14004|12|2022-01-06|
|7|[DayBreak-u/chineseocr_lite](https://github.com/DayBreak-u/chineseocr_lite)|超轻量级中文ocr，支持竖排文字识别, 支持ncnn、mnn、tnn推理 ( dbnet(1.8M) + crnn(2.5M) + anglenet(378KB)) 总模型仅4.7M |8430|12|2022-01-13|
|8|[CodingGay/BlackDex](https://github.com/CodingGay/BlackDex)|BlackDex is an Android unpack(dexdump) tool, it supports Android 5.0~12 and need not rely to any environment. BlackDex can run on any Android mobile phone or emulator, you can unpack APK File in sever ...|2917|12|2022-01-09|
|9|[zhongyang219/TrafficMonitor](https://github.com/zhongyang219/TrafficMonitor)|这是一个用于显示当前网速、CPU及内存利用率的桌面悬浮窗软件，并支持任务栏显示，支持更换皮肤。|15551|10|2022-01-13|
|10|[crisprss/BypassUserAdd](https://github.com/crisprss/BypassUserAdd)|通过反射DLL注入、Win API、C#、以及底层实现NetUserAdd方式实现BypassAV进行增加用户的功能,实现Cobalt Strike插件化|98|10|2022-01-13|
|11|[taichi-dev/taichi](https://github.com/taichi-dev/taichi)|Productive & portable high-performance programming in Python.|17811|9|2022-01-13|
|12|[Tencent/mars](https://github.com/Tencent/mars)|Mars is a cross-platform network component  developed by WeChat.|15950|9|2022-01-13|
|13|[envoyproxy/envoy](https://github.com/envoyproxy/envoy)|Cloud-native high-performance edge/middle/service proxy|18717|9|2022-01-13|
|14|[Tencent/ncnn](https://github.com/Tencent/ncnn)|ncnn is a high-performance neural network inference framework optimized for the mobile platform|13568|8|2022-01-13|
|15|[Tencent/matrix](https://github.com/Tencent/matrix)|Matrix is a plugin style, non-invasive APM system developed by WeChat.|9440|8|2021-12-22|
|16|[Tencent/Hippy](https://github.com/Tencent/Hippy)|Hippy is designed for Web developer to easily build cross-platform and high-performance awesome apps. 👏|6274|8|2022-01-13|
|17|[apache/incubator-brpc](https://github.com/apache/incubator-brpc)|Industrial-grade RPC framework used throughout Baidu, with 1,000,000+ instances and thousands kinds of services. "brpc" means "better RPC".|12796|8|2022-01-13|
|18|[alibaba/weex](https://github.com/alibaba/weex)|A framework for building Mobile cross-platform UI|17810|8|2021-12-09|
|19|[AaronFeng753/Waifu2x-Extension-GUI](https://github.com/AaronFeng753/Waifu2x-Extension-GUI)|Video, Image and GIF upscale/enlarge(Super-Resolution) and Video frame interpolation. Achieved with Waifu2x,  Real-ESRGAN, SRMD, RealSR, Anime4K, RIFE, CAIN, DAIN,  and ACNet.|5630|8|2022-01-07|
|20|[ossrs/srs](https://github.com/ossrs/srs)|SRS is a simple, high efficiency and realtime video server, supports RTMP, WebRTC, HLS, HTTP-FLV and SRT.|16973|7|2022-01-13|
|21|[MegEngine/MegEngine](https://github.com/MegEngine/MegEngine)|MegEngine 是一个快速、可拓展、易于使用且支持自动求导的深度学习框架|4106|6|2022-01-13|
|22|[barry-ran/QtScrcpy](https://github.com/barry-ran/QtScrcpy)|Android real-time display control software|5985|6|2022-01-10|
|23|[Tencent/Tendis](https://github.com/Tencent/Tendis)|Tendis is a high-performance distributed storage system fully compatible with the Redis protocol. |2313|6|2021-11-18|
|24|[tindy2013/subconverter](https://github.com/tindy2013/subconverter)|Utility to convert between various subscription format|5097|6|2022-01-13|
|25|[vesoft-inc/nebula](https://github.com/vesoft-inc/nebula)|  A distributed, fast open-source graph database featuring horizontal scalability and high availability|7043|6|2022-01-13|
|26|[alibaba/MNN](https://github.com/alibaba/MNN)|MNN is a blazing fast, lightweight deep learning framework, battle-tested by business-critical use cases in Alibaba|6348|6|2022-01-13|
|27|[Tencent/TNN](https://github.com/Tencent/TNN)|TNN: developed by Tencent Youtu Lab and Guangying Lab, a uniform deep learning inference framework for mobile、desktop and server. TNN is distinguished by several outstanding features, including its cr ...|3306|6|2022-01-13|
|28|[drogonframework/drogon](https://github.com/drogonframework/drogon)|Drogon: A C++14/17/20 based HTTP web application framework running on Linux/macOS/Unix/Windows|6714|5|2022-01-13|
|29|[kdrag0n/safetynet-fix](https://github.com/kdrag0n/safetynet-fix)|Universal fix for Google SafetyNet on Android devices with hardware attestation and unlocked bootloaders.|1953|5|2021-12-24|
|30|[EdVince/ClothingTransfer-NCNN](https://github.com/EdVince/ClothingTransfer-NCNN)|CT-Net, OpenPose, LIP_JPPNet, DensePose running with ncnn⚡服装迁移/虚拟试穿⚡ClothingTransfer/Virtual-Try-On⚡|140|5|2022-01-01|
|31|[swoole/swoole-src](https://github.com/swoole/swoole-src)|🚀 Coroutine-based concurrency library for PHP|17211|5|2022-01-13|
|32|[qinguoyi/TinyWebServer](https://github.com/qinguoyi/TinyWebServer)|:fire: Linux下C++轻量级Web服务器|4991|5|2022-01-07|
|33|[vnotex/vnote](https://github.com/vnotex/vnote)|A pleasant note-taking platform.|8858|5|2022-01-13|
|34|[Chaoses-Ib/IbEverythingExt](https://github.com/Chaoses-Ib/IbEverythingExt)|Everything 拼音搜索、快速选择扩展|622|5|2021-12-17|
|35|[TonyChen56/WeChatRobot](https://github.com/TonyChen56/WeChatRobot)|PC版微信机器人 微信Api、微信api、微信发卡机器人、微信聊天机器人 python微信api 微信接口 微信数据库解密|4166|5|2021-10-29|
|36|[wang-xinyu/tensorrtx](https://github.com/wang-xinyu/tensorrtx)|Implementation of popular deep learning networks with TensorRT network definition API|3579|5|2022-01-10|
|37|[TarsCloud/Tars](https://github.com/TarsCloud/Tars)|Tars is a high-performance RPC framework based on name service and Tars protocol, also integrated administration platform, and implemented hosting-service via flexible schedule.|9290|5|2022-01-13|
|38|[PaddlePaddle/Paddle-Lite](https://github.com/PaddlePaddle/Paddle-Lite)|Multi-platform high performance  deep learning inference engine (『飞桨』多平台高性能深度学习预测引擎）|5852|4|2022-01-13|
|39|[tuplex/tuplex](https://github.com/tuplex/tuplex)|Tuplex is a parallel big data processing framework that runs data science pipelines written in Python at the speed of compiled code. Tuplex has similar Python APIs to Apache Spark or Dask, but rather  ...|746|4|2022-01-13|
|40|[idea4good/GuiLite](https://github.com/idea4good/GuiLite)|✔️The smallest header-only GUI library(4 KLOC) for all platforms|5890|4|2021-08-01|
|41|[mindspore-ai/mindspore](https://github.com/mindspore-ai/mindspore)|MindSpore is a new open source deep learning training/inference framework that could be used for mobile, edge and cloud scenarios.|2666|4|2022-01-08|
|42|[OAID/TengineKit](https://github.com/OAID/TengineKit)|TengineKit - Free, Fast, Easy, Real-Time Face Detection & Face Landmarks & Face Attributes & Hand Detection & Hand Landmarks & Body Detection & Body Landmarks &  Iris Landmarks & Yolov5 SDK On Mobile.|2129|4|2021-10-18|
|43|[KwaiAppTeam/KOOM](https://github.com/KwaiAppTeam/KOOM)|KOOM is an OOM killer on mobile platform by Kwai.|2303|4|2021-11-16|
|44|[Ewenwan/MVision](https://github.com/Ewenwan/MVision)|机器人视觉 移动机器人 VS-SLAM ORB-SLAM2 深度学习目标检测 yolov3 行为检测 opencv  PCL 机器学习 无人驾驶|6195|4|2021-07-29|
|45|[Serial-Studio/Serial-Studio](https://github.com/Serial-Studio/Serial-Studio)|Multi-purpose serial data visualization & processing program|1977|4|2022-01-11|
|46|[Tencent/rapidjson](https://github.com/Tencent/rapidjson)|A fast JSON parser/generator for C++ with both SAX/DOM style API|11631|4|2022-01-08|
|47|[XiaoMi/mace](https://github.com/XiaoMi/mace)|MACE is a deep learning inference framework optimized for mobile heterogeneous computing platforms.|4551|4|2022-01-13|
|48|[Tencent/flare](https://github.com/Tencent/flare)| Flare是广泛投产于腾讯广告后台的现代化C++开发框架，包含了基础库、RPC、各种客户端等。主要特点为易用性强、长尾延迟低。 |831|4|2021-12-24|
|49|[wenet-e2e/wenet](https://github.com/wenet-e2e/wenet)|Production First and Production Ready End-to-End Speech Recognition Toolkit|1724|4|2022-01-12|
|50|[Tencent/libco](https://github.com/Tencent/libco)|libco is a coroutine library which is widely used in wechat  back-end service. It has been running on tens of thousands of machines since 2013.|6819|4|2021-12-03|
|51|[shouxieai/tensorRT_Pro](https://github.com/shouxieai/tensorRT_Pro)|C++ library based on tensorrt integration|583|3|2021-12-15|
|52|[alphacep/vosk-api](https://github.com/alphacep/vosk-api)|Offline speech recognition API for Android, iOS, Raspberry Pi and servers with Python, Java, C# and Node|2746|3|2022-01-12|
|53|[ZLMediaKit/ZLMediaKit](https://github.com/ZLMediaKit/ZLMediaKit)|WebRTC/RTSP/RTMP/HTTP/HLS/HTTP-FLV/WebSocket-FLV/HTTP-TS/HTTP-fMP4/WebSocket-TS/WebSocket-fMP4/GB28181 server and client framework based on C++11|5475|3|2022-01-13|
|54|[sonic-pi-net/sonic-pi](https://github.com/sonic-pi-net/sonic-pi)|Code. Music. Live.|8803|3|2022-01-06|
|55|[szad670401/HyperLPR](https://github.com/szad670401/HyperLPR)|基于深度学习高性能中文车牌识别 High Performance Chinese License Plate Recognition Framework.|4429|3|2022-01-13|
|56|[wangyu-/udp2raw](https://github.com/wangyu-/udp2raw)|A Tunnel which Turns UDP Traffic into Encrypted UDP/FakeTCP/ICMP Traffic by using Raw Socket,helps you Bypass UDP FireWalls(or Unstable UDP Environment)|4890|3|2021-12-09|
|57|[google/sentencepiece](https://github.com/google/sentencepiece)|Unsupervised text tokenizer for Neural Network-based text generation.|5595|3|2022-01-12|
|58|[OAID/Tengine](https://github.com/OAID/Tengine)|Tengine is a lite, high performance, modular inference engine for embedded device |4077|3|2022-01-13|
|59|[Alinshans/MyTinySTL](https://github.com/Alinshans/MyTinySTL)|Achieve a tiny STL in C++11|4994|3|2021-12-29|
|60|[snowie2000/mactype](https://github.com/snowie2000/mactype)|Better font rendering for Windows.|6948|3|2021-12-06|
|61|[weolar/miniblink49](https://github.com/weolar/miniblink49)|a lighter, faster browser kernel of blink to integrate HTML UI in your app. 一个小巧、轻量的浏览器内核，用来取代wke和libcef|5465|3|2021-12-27|
|62|[idealvin/cocoyaxi](https://github.com/idealvin/cocoyaxi)|A C++ library from the Namake planet. (go-style coroutine, flag, logging, unit-test, and more)|2306|3|2022-01-11|
|63|[wfrest/wfrest](https://github.com/wfrest/wfrest)|C++ Web Framework REST API|261|3|2022-01-13|
|64|[WasmEdge/WasmEdge](https://github.com/WasmEdge/WasmEdge)|WasmEdge is a lightweight, high-performance, and extensible WebAssembly runtime for cloud native, edge, and decentralized applications. It powers serverless apps, embedded functions, microservices, sm ...|2524|3|2022-01-13|
|65|[feiyangqingyun/QWidgetDemo](https://github.com/feiyangqingyun/QWidgetDemo)|Qt编写的一些开源的demo，预计会有100多个，一直持续更新完善，代码简洁易懂注释详细，每个都是独立项目，非常适合初学者，代码随意传播使用，拒绝打赏和捐赠，欢迎留言评论！|2848|3|2022-01-13|
|66|[openppl-public/ppl.nn](https://github.com/openppl-public/ppl.nn)|A primitive library for neural network|644|3|2022-01-13|
|67|[xournalpp/xournalpp](https://github.com/xournalpp/xournalpp)|Xournal++ is a handwriting notetaking software with PDF annotation support. Written in C++ with GTK3, supporting Linux (e.g. Ubuntu, Debian, Arch, SUSE), macOS and Windows 10. Supports pen input from  ...|5485|2|2022-01-13|
|68|[duilib/duilib](https://github.com/duilib/duilib)|-|4645|2|2022-01-11|
|69|[seetafaceengine/SeetaFace2](https://github.com/seetafaceengine/SeetaFace2)|SeetaFace 2: open source, full stack face recognization toolkit.|1894|2|2021-09-28|
|70|[OAID/AutoKernel](https://github.com/OAID/AutoKernel)|AutoKernel 是一个简单易用，低门槛的自动算子优化工具，提高深度学习算法部署效率。|692|2|2021-09-27|
|71|[hrydgard/ppsspp](https://github.com/hrydgard/ppsspp)|A PSP emulator for Android, Windows, Mac and Linux, written in C++. Want to contribute? Join us on Discord at https://discord.gg/5NJB6dD or just send pull requests / issues. For discussion use the for ...|6833|2|2022-01-13|
|72|[githubhaohao/NDK_OpenGLES_3_0](https://github.com/githubhaohao/NDK_OpenGLES_3_0)|Android OpenGL ES 3.0 从入门到精通系统性学习教程|1542|2|2021-11-17|
|73|[TianZerL/Anime4KCPP](https://github.com/TianZerL/Anime4KCPP)|A high performance anime upscaler|1226|2|2021-12-10|
|74|[PointCloudLibrary/pcl](https://github.com/PointCloudLibrary/pcl)|Point Cloud Library (PCL)|6976|2|2022-01-13|
|75|[MiRoboticsLab/cyberdog_ros2](https://github.com/MiRoboticsLab/cyberdog_ros2)|ROS 2 packages for CyberDog|240|2|2021-12-30|
|76|[Tencent/Hardcoder](https://github.com/Tencent/Hardcoder)|Hardcoder is a solution which allows Android APP and Android System to communicate with each other directly, solving the problem that Android APP could only use system standard API rather than the har ...|1947|2|2021-11-11|
|77|[SwiftLaTeX/SwiftLaTeX](https://github.com/SwiftLaTeX/SwiftLaTeX)|SwiftLaTeX, a WYSIWYG Browser-based LaTeX Editor |1671|2|2021-11-03|
|78|[baidu/braft](https://github.com/baidu/braft)|An industrial-grade C++ implementation of RAFT consensus algorithm based on brpc,  widely used inside Baidu to build highly-available distributed systems.|3025|2|2022-01-04|
|79|[sogou/srpc](https://github.com/sogou/srpc)|RPC based on C++ Workflow. Supports Baidu bRPC, Tencent tRPC, thrift protocols.|1072|2|2022-01-13|
|80|[ZhuYanzhen1/miniFOC](https://github.com/ZhuYanzhen1/miniFOC)|你还在为有刷电机的高噪声、低响应速度和低寿命而烦恼吗？这个项目是一个20块钱就能搞定的FOC无刷电机控制方案！This project is a FOC (Field Oriented Control) BLDC Motor control scheme that can be done for 3$!|277|2|2021-11-08|
|81|[OpenAtomFoundation/pika](https://github.com/OpenAtomFoundation/pika)|Pika is a nosql compatible with redis, it is developed by Qihoo's DBA and infrastructure team|4485|2|2022-01-03|
|82|[sass/node-sass](https://github.com/sass/node-sass)|:rainbow: Node.js bindings to libsass|8014|2|2021-12-29|
|83|[BlueMatthew/WechatExporter](https://github.com/BlueMatthew/WechatExporter)|Wechat Chat History Exporter 微信聊天记录导出程序|1072|2|2022-01-06|
|84|[peng-zhihui/Ctrl-FOC-Lite](https://github.com/peng-zhihui/Ctrl-FOC-Lite)|-|323|2|2021-08-07|
|85|[richardchien/modern-cmake-by-example](https://github.com/richardchien/modern-cmake-by-example)|IPADS 实验室新人培训第二讲（2021.11.3）：CMake|170|2|2021-11-04|
|86|[ic005k/QtOpenCoreConfig](https://github.com/ic005k/QtOpenCoreConfig)|OpenCore Auxiliary Tools OpenCore Configurator  OCAT|929|2|2022-01-12|
|87|[Tencent/plato](https://github.com/Tencent/plato)|腾讯高性能分布式图计算框架Plato|1704|2|2021-08-14|
|88|[Oneflow-Inc/oneflow](https://github.com/Oneflow-Inc/oneflow)|OneFlow is a performance-centered and open-source deep learning framework.|2950|2|2022-01-13|
|89|[alibaba/euler](https://github.com/alibaba/euler)|A distributed graph deep learning framework.|2710|2|2021-12-30|
|90|[ideawu/ssdb](https://github.com/ideawu/ssdb)|SSDB - A fast NoSQL database, an alternative to Redis|7727|2|2021-12-22|
|91|[0x727/SqlKnife_0x727](https://github.com/0x727/SqlKnife_0x727)|适合在命令行中使用的轻巧的SQL Server数据库安全检测工具|330|2|2021-10-23|
|92|[Tencent/Forward](https://github.com/Tencent/Forward)|A library for high performance deep learning inference on NVIDIA GPUs. |486|2|2021-11-30|
|93|[wecooperate/iMonitor](https://github.com/wecooperate/iMonitor)|iMonitor（冰镜 - 终端行为分析系统）|114|2|2022-01-02|
|94|[microsoft/BlingFire](https://github.com/microsoft/BlingFire)|A lightning fast Finite State machine and REgular expression manipulation library.|1588|2|2021-11-17|
|95|[me115/design_patterns](https://github.com/me115/design_patterns)|图说设计模式|5634|2|2021-08-10|
|96|[BlackINT3/OpenArk](https://github.com/BlackINT3/OpenArk)|OpenArk is an open source anti-rookit(ARK) tool for Windows. |2284|2|2021-11-25|
|97|[SpriteOvO/Telegram-Anti-Revoke](https://github.com/SpriteOvO/Telegram-Anti-Revoke)|Telegram anti-revoke plugin|1499|2|2021-12-10|
|98|[visualfc/liteide](https://github.com/visualfc/liteide)|LiteIDE is a simple, open source, cross-platform Go IDE. |6693|2|2022-01-12|
|99|[opencurve/curve](https://github.com/opencurve/curve)|Curve is a better-used cloud-native SDS storage system, featured with high performance, easy operation, cloud native. Curve is composed with CurveBS and CurveFS. CurveBS provides block devices based o ...|934|2|2022-01-13|
|100|[kungfu-origin/kungfu](https://github.com/kungfu-origin/kungfu)|Kungfu Trader|2549|2|2021-11-16|
|101|[yuanming-hu/taichi_mpm](https://github.com/yuanming-hu/taichi_mpm)|High-performance moving least squares material point method (MLS-MPM) solver. (ACM Transactions on Graphics, SIGGRAPH 2018)|2047|2|2021-11-30|
|102|[balloonwj/flamingo](https://github.com/balloonwj/flamingo)|flamingo 一款高性能轻量级开源即时通讯软件|2855|2|2021-10-20|
|103|[wangyu-/UDPspeeder](https://github.com/wangyu-/UDPspeeder)|A Tunnel which Improves your Network Quality on a High-latency Lossy Link by using Forward Error Correction, possible for All Traffics(TCP/UDP/ICMP)|3726|2|2021-12-09|
|104|[p-ranav/indicators](https://github.com/p-ranav/indicators)|Activity Indicators for Modern C++|1882|2|2021-10-28|
|105|[alibaba/graph-learn](https://github.com/alibaba/graph-learn)|An Industrial Graph Neural Network Framework|1004|2|2021-12-24|
|106|[sylar-yin/sylar](https://github.com/sylar-yin/sylar)|C++高性能分布式服务器框架,webserver,websocket server,自定义tcp_server（包含日志模块，配置模块，线程模块，协程模块，协程调度模块，io协程调度模块，hook模块，socket模块，bytearray序列化，http模块，TcpServer模块，Websocket模块，Https模块等, Smtp邮件模块, MySQL, SQLite3, ORM,Redis, ...|1512|2|2021-12-10|
|107|[MistEO/MeoAssistantArknights](https://github.com/MistEO/MeoAssistantArknights)|明日方舟助手，自动刷图、智能基建换班，全日常一键长草！|361|2|2022-01-12|
|108|[metartc/yangwebrtc](https://github.com/metartc/yangwebrtc)|Webrtc Libary for PC, non-google lib|211|2|2022-01-13|
|109|[OpenIntelWireless/IntelBluetoothFirmware](https://github.com/OpenIntelWireless/IntelBluetoothFirmware)|Intel Bluetooth Drivers for macOS|1849|2|2022-01-01|
|110|[HKUST-Aerial-Robotics/VINS-Mono](https://github.com/HKUST-Aerial-Robotics/VINS-Mono)|A Robust and Versatile Monocular Visual-Inertial State Estimator|3360|2|2021-12-25|
|111|[qicosmos/cinatra](https://github.com/qicosmos/cinatra)|modern c++(c++17), cross-platform, header-only, easy to use http framework|1226|1|2022-01-13|
|112|[blinker-iot/blinker-library](https://github.com/blinker-iot/blinker-library)|An IoT Solution,Blinker library for embedded hardware. Works with Arduino, ESP8266, ESP32.|1645|1|2022-01-06|
|113|[acidanthera/BrcmPatchRAM](https://github.com/acidanthera/BrcmPatchRAM)|-|606|1|2021-11-02|
|114|[anhkgg/SuperRDP](https://github.com/anhkgg/SuperRDP)|Super RDPWrap|517|1|2022-01-13|
|115|[huoji120/CobaltStrikeDetected](https://github.com/huoji120/CobaltStrikeDetected)|40行代码检测到大部分CobaltStrike的shellcode|188|1|2021-07-25|
|116|[mhogomchungu/media-downloader](https://github.com/mhogomchungu/media-downloader)|Media Downloader is a Qt/C++ front end to youtube-dl|261|1|2022-01-11|
|117|[ysc3839/FontMod](https://github.com/ysc3839/FontMod)|Simple hook tool to change Win32 program font.|1122|1|2021-08-09|
|118|[namazso/OpenHashTab](https://github.com/namazso/OpenHashTab)|📝 File hashing and checking shell extension|878|1|2021-11-30|
|119|[emilybache/GildedRose-Refactoring-Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata)|Starting code for the GildedRose Refactoring Kata in many programming languages.|2069|1|2022-01-04|
|120|[USTC-Hackergame/hackergame2020-writeups](https://github.com/USTC-Hackergame/hackergame2020-writeups)|中国科学技术大学第七届信息安全大赛的官方与非官方题解|462|1|2021-08-16|
|121|[Netis/packet-agent](https://github.com/Netis/packet-agent)|A toolset for network packet capture in Cloud/Kubernetes and Virtualized environment.|891|1|2021-12-22|
|122|[zhongyang219/MusicPlayer2](https://github.com/zhongyang219/MusicPlayer2)|这是一款可以播放常见音频格式的音频播放器。支持歌词显示、歌词卡拉OK样式显示、歌词在线下载、歌词编辑、歌曲标签识别、Win10小娜搜索显示歌词、频谱分析、音效设置、任务栏缩略图按钮、主题颜色等功能。 播放内核为BASS音频库(V2.4)。|1410|1|2021-12-05|
|123|[rvbust/Vis](https://github.com/rvbust/Vis)|Interactive & Asynchronous 3D Visualization Tool|96|1|2021-08-18|
|124|[openthread/openthread](https://github.com/openthread/openthread)|OpenThread released by Google is an open-source implementation of the Thread networking protocol|2668|1|2022-01-13|
|125|[FISCO-BCOS/FISCO-BCOS](https://github.com/FISCO-BCOS/FISCO-BCOS)|FISCO BCOS是由微众牵头的金链盟主导研发、对外开源、安全可控的企业级金融区块链底层技术平台。 单链配置下，性能TPS可达万级。提供群组架构、并行计算、分布式存储、可插拔的共识机制、隐私保护算法、支持全链路国密算法等诸多特性。 经过多个机构、多个应用，长时间在生产环境中的实践检验，具备金融级的高性能、高可用性及高安全性。FISCO BCOS is a secure and reliable  ...|1563|1|2022-01-13|
|126|[taptap/perf-doctor](https://github.com/taptap/perf-doctor)|性能医生，手机应用性能测试工具|146|1|2022-01-10|
|127|[helio-fm/helio-workstation](https://github.com/helio-fm/helio-workstation)|One music sequencer for all major platforms, desktop and mobile|2276|1|2022-01-10|
|128|[kaniol-lck/modmanager](https://github.com/kaniol-lck/modmanager)|A Qt-based mod manager for minecraft.|131|1|2022-01-13|
|129|[guojianyang/cv-detect-robot](https://github.com/guojianyang/cv-detect-robot)|🔥🔥🔥🔥🔥🔥yolov5+YOLOX+yolo+tensorRT+ros+deepstream+jetson+nano+TX2+NX for High-performance deployment(高性能部署)|110|1|2022-01-09|
|130|[cixingguangming55555/wechat-bot](https://github.com/cixingguangming55555/wechat-bot)|带二次开发接口的PC微信聊天机器人|919|1|2022-01-11|
|131|[SmartKeyerror/reading-source-code-of-leveldb-1.23](https://github.com/SmartKeyerror/reading-source-code-of-leveldb-1.23)|leveldb 源码阅读，分析 DB 运作流程与 WAL、SSTable 等文件格式与 Compaction 过程。|168|1|2021-09-24|
|132|[daliansky/XiaoXinPro-13-hackintosh](https://github.com/daliansky/XiaoXinPro-13-hackintosh)|Lenovo XiaoXin Pro 13 2019 Hackintosh|719|1|2021-12-11|
|133|[tencentyun/TRTCSDK](https://github.com/tencentyun/TRTCSDK)|腾讯云TRTC音视频服务，国内下载镜像：|1121|1|2022-01-12|
|134|[didi/AoE](https://github.com/didi/AoE)|AoE (AI on Edge，终端智能，边缘计算) 是一个终端侧AI集成运行时环境 (IRE)，帮助开发者提升效率。|803|1|2021-09-28|
|135|[AllentDan/LibtorchSegmentation](https://github.com/AllentDan/LibtorchSegmentation)|A c++ trainable semantic segmentation library based on libtorch (pytorch c++). Backbone: VGG, ResNet, ResNext. Architecture: FPN, U-Net, PAN, LinkNet, PSPNet, DeepLab-V3, DeepLab-V3+ by now.|200|1|2021-11-17|
|136|[amov-lab/Prometheus](https://github.com/amov-lab/Prometheus)|Open source software for autonomous drones.|995|1|2022-01-05|
|137|[SeetaFace6Open/index](https://github.com/SeetaFace6Open/index)|-|265|1|2021-11-29|
|138|[wagiminator/ATmega-Soldering-Station](https://github.com/wagiminator/ATmega-Soldering-Station)|T12 Quick Heating Soldering Station|535|1|2021-08-24|
|139|[hxhb/HotPatcher](https://github.com/hxhb/HotPatcher)|Unreal Engine hot update manage and package plugin.|420|1|2022-01-13|
|140|[AmazingPP/subVerison_GTAV_Hack](https://github.com/AmazingPP/subVerison_GTAV_Hack)|subVerison重置版——GTA5外置修改器|304|1|2021-08-15|
|141|[EdVince/PSGAN-NCNN](https://github.com/EdVince/PSGAN-NCNN)|PSGAN running with ncnn⚡妆容迁移/仿妆⚡Imitation Makeup/Makeup Transfer⚡|108|1|2021-10-30|
|142|[rizonesoft/Notepad3](https://github.com/rizonesoft/Notepad3)|Notepad like text editor based on the Scintilla source code. Notepad3 based on code from Notepad2 and MiniPath on code from metapath. Download Notepad3:|2279|1|2022-01-12|
|143|[RapidAI/RapidOCR](https://github.com/RapidAI/RapidOCR)|A cross platform OCR Library based on PaddleOCR & OnnxRuntime|274|1|2021-12-24|
|144|[Tencent/Taitank](https://github.com/Tencent/Taitank)|Taitank is a cross platform lightweight flex layout engine implemented in C++.|386|1|2021-07-21|
|145|[miguelbalboa/rfid](https://github.com/miguelbalboa/rfid)|Arduino RFID Library for MFRC522|2207|1|2022-01-07|
|146|[OAID/TengineGst](https://github.com/OAID/TengineGst)|TengineGst is a streaming media analytics framework, based on GStreamer multimedia framework, for creating varied complex media analytics pipelines. It ensures pipeline interoperability and provides o ...|59|1|2021-10-27|
|147|[Tencent/DCache](https://github.com/Tencent/DCache)|A distributed in-memory NOSQL system based on TARS framework, support LRU algorithm and data persists on  back-end database. Users can easily deploy, publish, and scale services on the web interface.|669|1|2021-11-25|
|148|[qiang/Riru-ModuleFridaGadget](https://github.com/qiang/Riru-ModuleFridaGadget)|一个magisk 的模块，简化版，依赖 riru，能够简单的hook，并且加载动态库，目前用来加载 frida 的gadget 库，从而使hook脱离命令行和server，并且能够在多进程中加载|77|1|2021-12-18|
|149|[simplefoc/Arduino-FOC](https://github.com/simplefoc/Arduino-FOC)|Arduino FOC for BLDC and Stepper motors - Arduino Based Field Oriented Control Algorithm Library|820|1|2022-01-08|
|150|[yedf2/handy](https://github.com/yedf2/handy)|🔥简洁易用的C++11网络库 / 支持单机千万并发连接 / a simple C++11 network server framework|3851|1|2021-10-11|
|151|[ApsaraDB/PolarDB-FileSystem](https://github.com/ApsaraDB/PolarDB-FileSystem)|-|80|1|2021-11-10|
|152|[githubhaohao/OpenGLCamera2](https://github.com/githubhaohao/OpenGLCamera2)|🔥 Android OpenGL Camera 2.0  实现 30 种滤镜和抖音特效|562|1|2021-10-11|
|153|[soarqin/D2RMH](https://github.com/soarqin/D2RMH)|Diablo II Resurrected map revealing tool.|118|1|2022-01-11|
|154|[yanyiwu/cppjieba](https://github.com/yanyiwu/cppjieba)|"结巴"中文分词的C++版本|2090|1|2022-01-09|
|155|[xiangli0608/cartographer_detailed_comments_ws](https://github.com/xiangli0608/cartographer_detailed_comments_ws)|cartographer work space with detailed comments|165|1|2022-01-12|
|156|[netease-im/NIM_Duilib_Framework](https://github.com/netease-im/NIM_Duilib_Framework)|网易云信Windows应用界面开发框架（基于Duilib）。招人招人，windows/mac/duilib/qt/electron http://mobile.bole.netease.com/bole/boleDetail?id=19904&employeeId=510064bce318835c&key=all&type=2&from=timeline|1291|1|2022-01-04|
|157|[AntiMicro/antimicro](https://github.com/AntiMicro/antimicro)|Graphical program used to map keyboard buttons and mouse controls to a gamepad. Useful for playing games with no gamepad support|1515|1|2021-09-19|
|158|[Ubpa/Utopia](https://github.com/Ubpa/Utopia)|Utopia Game Engine 无境游戏引擎|269|1|2022-01-13|
|159|[Tencent/yadcc](https://github.com/Tencent/yadcc)|Yet Another Distributed C++ Compiler. yadcc是一套腾讯广告自研的分布式编译系统，用于支撑腾讯广告的日常开发及流水线。相对于已有的同类解决方案，我们针对实际的工业生产环境做了性能、可靠性、易用性等方面优化。|206|1|2021-12-26|
|160|[DustinWatts/esp32-touchdown](https://github.com/DustinWatts/esp32-touchdown)|ESP32 TouchDown|269|1|2021-09-10|
|161|[ZJU-FAST-Lab/ego-planner-swarm](https://github.com/ZJU-FAST-Lab/ego-planner-swarm)|An efficient single/multi-agent trajectory planner for multicopters.|279|1|2021-11-18|
|162|[rime/librime](https://github.com/rime/librime)|Rime Input Method Engine, the core library|2155|1|2022-01-13|
|163|[shangjingbo1226/AutoPhrase](https://github.com/shangjingbo1226/AutoPhrase)|AutoPhrase: Automated Phrase Mining from Massive Text Corpora|970|1|2021-11-18|
|164|[facebookresearch/SparseConvNet](https://github.com/facebookresearch/SparseConvNet)|Submanifold sparse convolutional networks|1584|1|2022-01-06|
|165|[liuchuo/PAT](https://github.com/liuchuo/PAT)|🍭 浙江大学PAT题解(C/C++/Java/Python) - 努力成为萌萌的程序媛～|2768|1|2021-10-08|
|166|[abcz316/linuxKernelRoot](https://github.com/abcz316/linuxKernelRoot)|新一代root，跟面具完全不同思路，摆脱面具被检测的弱点，完美隐藏root功能，挑战全网root检测手段，兼容安卓APP直接JNI调用，稳定、流畅、不闪退。|122|1|2021-12-04|
|167|[ErosZy/WXInlinePlayer](https://github.com/ErosZy/WXInlinePlayer)|🤟Super fast H.264/H.265 FLV player|1094|1|2021-09-16|
|168|[devilsen/CZXing](https://github.com/devilsen/CZXing)|C++ port of ZXing and ZBar for Android.|951|1|2021-08-29|
|169|[greg7mdp/parallel-hashmap](https://github.com/greg7mdp/parallel-hashmap)|A family of header-only, very fast and memory-friendly hashmap and btree containers.|1242|1|2021-12-27|
|170|[Xtra-Computing/thundersvm](https://github.com/Xtra-Computing/thundersvm)|ThunderSVM: A Fast SVM Library on GPUs and CPUs|1362|1|2022-01-07|
|171|[0x727/CloneX_0x727](https://github.com/0x727/CloneX_0x727)|进行克隆用户、添加用户等账户防护安全检测的轻巧工具|129|1|2021-09-03|
|172|[zenustech/zeno](https://github.com/zenustech/zeno)|ZEn NOde system|368|1|2022-01-13|
|173|[romkatv/gitstatus](https://github.com/romkatv/gitstatus)|Git status for Bash and Zsh prompt|1232|1|2022-01-04|
|174|[Kitt-AI/snowboy](https://github.com/Kitt-AI/snowboy)|Future versions with model training module will be maintained through a forked version here: https://github.com/seasalt-ai/snowboy|2659|1|2021-10-13|
|175|[hzeller/rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix)|Controlling up to three chains of 64x64, 32x32, 16x32 or similar RGB LED displays using Raspberry Pi GPIO|2577|1|2022-01-09|
|176|[ToKiNoBug/SlopeCraft](https://github.com/ToKiNoBug/SlopeCraft)|Get your map pixel art in minecraft|221|1|2022-01-12|
|177|[eritpchy/Fingerprint-pay-magisk-wechat](https://github.com/eritpchy/Fingerprint-pay-magisk-wechat)|微信指纹支付 (Fingerprint pay for WeChat)|557|1|2021-09-01|
|178|[anhkgg/SuperDllHijack](https://github.com/anhkgg/SuperDllHijack)|SuperDllHijack：A general DLL hijack technology, don't need to manually export the same function interface of the DLL, so easy! 一种通用Dll劫持技术，不再需要手工导出Dll的函数接口了|615|1|2021-11-10|
|179|[ZLMediaKit/ZLToolKit](https://github.com/ZLMediaKit/ZLToolKit)|一个基于C++11的轻量级网络框架，基于线程池技术可以实现大并发网络IO|1083|1|2022-01-13|
|180|[DragonQuestHero/Kernel-Anit-Anit-Debug-Plugins](https://github.com/DragonQuestHero/Kernel-Anit-Anit-Debug-Plugins)|Kernel Anit Anit Debug Plugins 内核反反调试插件|260|1|2021-08-31|
|181|[p-ranav/tabulate](https://github.com/p-ranav/tabulate)|Table Maker for Modern C++|1113|1|2021-10-07|
|182|[0x727/ShuiYing_0x727](https://github.com/0x727/ShuiYing_0x727)|检测域环境内，域机器的本地管理组成员是否存在弱口令和通用口令，对域用户的权限分配以及域内委派查询|210|1|2021-08-10|
|183|[HKUST-Aerial-Robotics/Teach-Repeat-Replan](https://github.com/HKUST-Aerial-Robotics/Teach-Repeat-Replan)|Teach-Repeat-Replan: A Complete and Robust System for Aggressive Flight in Complex Environments|542|1|2021-10-19|
|184|[Tencent/sluaunreal](https://github.com/Tencent/sluaunreal)|lua dev plugin for unreal engine 4|1369|1|2021-12-02|
|185|[iwxyi/Bilibili-MagicalDanmaku](https://github.com/iwxyi/Bilibili-MagicalDanmaku)|【神奇弹幕】哔哩哔哩直播万能机器人，弹幕姬+答谢姬+回复姬+点歌姬+各种小骚操作，目前唯一可编程机器人|255|1|2022-01-07|
|186|[msnh2012/Msnhnet](https://github.com/msnh2012/Msnhnet)|🔥 (yolov3 yolov4 yolov5 unet ...)A mini pytorch inference framework which inspired from darknet.|558|1|2021-10-23|
|187|[joyieldInc/predixy](https://github.com/joyieldInc/predixy)|A high performance and fully featured proxy for redis, support redis sentinel and redis cluster|1064|1|2021-10-01|
|188|[xyz347/xpack](https://github.com/xyz347/xpack)|convert json/xml/bson to c++ struct|301|1|2022-01-13|
|189|[downdemo/Cpp-Concurrency-in-Action-2ed](https://github.com/downdemo/Cpp-Concurrency-in-Action-2ed)|C++ Concurrency in Action 2ed 笔记：C++11/14/17/20 多线程，掌握操作系统原理，解锁并发编程技术|671|1|2022-01-08|
|190|[Syencil/tensorRT](https://github.com/Syencil/tensorRT)|TensorRT-7 Network Lib 包括常用目标检测、关键点检测、人脸检测、OCR等 可训练自己数据|444|1|2021-07-17|
|191|[BYVoid/OpenCC](https://github.com/BYVoid/OpenCC)|Conversion between Traditional and Simplified Chinese|6034|1|2021-12-18|
|192|[M2Team/NSudo](https://github.com/M2Team/NSudo)|Series of System Administration Tools|1224|1|2021-11-18|
|193|[NVIDIA-Merlin/HugeCTR](https://github.com/NVIDIA-Merlin/HugeCTR)|HugeCTR is a high efficiency GPU framework designed for Click-Through-Rate (CTR) estimating training|548|1|2022-01-11|
|194|[kothariji/competitive-programming](https://github.com/kothariji/competitive-programming)|Hello Programmers :computer: , A one-stop Destination✏️✏️ for all your Competitive Programming Resources.📗📕    Refer CONTRIBUTING.md for contributions|349|1|2021-11-02|
|195|[Tatsu-syo/noMeiryoUI](https://github.com/Tatsu-syo/noMeiryoUI)|No!! MeiryoUI is Windows system font setting tool on Windows 8.1/10/11.|1814|1|2021-12-19|
|196|[ToanTech/Deng-s-foc-controller](https://github.com/ToanTech/Deng-s-foc-controller)|灯哥开源 FOC 双路迷你无刷电机驱动|316|1|2021-12-31|
|197|[fanvanzh/3dtiles](https://github.com/fanvanzh/3dtiles)|The fastest tools for 3dtiles convert in the world!|1100|1|2022-01-04|
|198|[Zhuagenborn/Plants-vs.-Zombies-Online-Battle](https://github.com/Zhuagenborn/Plants-vs.-Zombies-Online-Battle)|🎮 Plants vs. Zombies multiplayer battle, developed via reverse engineering, inline hook and dynamic-link library injection. Two online players defend and attack as the plant and zombie respectively.|284|1|2022-01-01|
|199|[vczh/GacUIBlog](https://github.com/vczh/GacUIBlog)|记录 GacUI 开发10年来背后的故事，以及对架构设计的考量。|320|1|2021-12-20|
|200|[WaykiChain/WaykiChain](https://github.com/WaykiChain/WaykiChain)|Public Blockchain as a Decentralized Finance Infrastructure Service Platform|1112|1|2021-12-11|

<div align="center">
    <p><sub>↓ -- 感谢读者 -- ↓</sub></p>
    榜单持续更新，如有帮助请加星收藏，方便后续浏览，感谢你的支持！
</div>

<br/>

<div align="center"><a href="https://github.com/GrowingGit/GitHub-Chinese-Top-Charts#github中文排行榜">返回目录</a> • <a href="/content/docs/feedback.md">问题反馈</a></div>
